<?php
$mainMenu = Menu::get('Главное меню');
$currentLang = app()->getLocale();

$langs = \App\Models\Langs::getLangsWithTitle();
?>
<header>
    <div class="container">
        <div class="logowrp">
            <a href="{{route('main')}}" class="logo">
                <img src="{{asset('/img/logo.svg')}}" alt=""></a>
        </div>
        <div class="centerMenu">
            <a class="catalogLnk">
                <img src="{{asset('/img/catalog.svg')}}" alt=""> {{__('all catalog')}}
            </a>
            @foreach($mainMenu as $item)
                <?php
                    if($item['type'] == \App\Models\Menu::TYPE_ARBITRARY){
                        $name = $item['name'];

                        foreach ($item['translations'] as $trans){
                            if($trans['lang'] === $currentLang){
                                $name = $trans['name'];
                                $url = $trans['url'];
                            }
                        }
                    } else {
                        $modelRel = $item[\App\Models\Menu::getTypesModel()[$item['type']]['rel']];

                        if(is_null($modelRel)){
                            continue;
                        }

                        $url = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['path'];

                        if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                            $url = '/' . $currentLang . '/' . $url;
                        }

                        $name = $modelRel[\App\Models\Menu::getTypesModel()[$item['type']]['name']];

                        foreach ($item['translations'] as $trans){
                            if($trans['lang'] === $currentLang){
                                $name = $trans['name'];
                            }
                        }
                    }
                ?>

                @if(count($item['children']) == 0)
                    <a href="{{$url}}" class="normalLink mh">{{$name}}</a>
                @else
                    <span data-href="#" class="dropDown mh">
                        <a class="normalLink" href="{{$url}}">{{$name}} <span class="ic-chewrondown"></span></a>
                        <div class="expand">
                            @foreach($item['children'] as $item2)
                                <?php
                                if($item2['type'] == \App\Models\Menu::TYPE_ARBITRARY){
                                    $name = $item2['name'];

                                    foreach ($item2['translations'] as $trans){
                                        if($trans['lang'] === $currentLang){
                                            $name = $trans['name'];
                                            $url = $trans['url'];
                                        }
                                    }
                                } else {
                                    $modelRel = $item2[\App\Models\Menu::getTypesModel()[$item2['type']]['rel']];

                                    if(is_null($modelRel)){
                                        continue;
                                    }

                                    $url = \App\Models\Menu::getTypesModel()[$item2['type']]['url_prefix'] . $modelRel['path'];

                                    if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                                        $url = '/' . $currentLang . '/' . $url;
                                    }

                                    $name = $modelRel[\App\Models\Menu::getTypesModel()[$item2['type']]['name']];

                                    foreach ($item2['translations'] as $trans){
                                        if($trans['lang'] === $currentLang){
                                            $name = $trans['name'];
                                        }
                                    }
                                }
                                ?>

                                <a href="{{$url}}">{{$name}}</a>
                            @endforeach
                        </div>
                    </span>
                @endif

            @endforeach
        </div>
        <div class="langWrp">
            <span class="ddCaption">{{$langs[app()->getLocale()]}} <span class="ic-chewrondown"></span></span>
            <div class="expand">
                @foreach($langs as $key => $l)
                    @if($key !== app()->getLocale())
                        <a href="{{route('setlocale',['lang' => $key])}}">{{$l}}</a>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="mobBurger">
            <a href="#">
                <img src="/img/catalog.svg" alt="">
            </a>
        </div>
    </div>
</header>
