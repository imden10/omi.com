<?php
$catalogMenu = Menu::get('Весь каталог');
$currentLang = app()->getLocale();
$infoMenu = Menu::get('Инфо-страницы');
$servicesMenu = Menu::get('Мобильное меню Услуги');
$langs = \App\Models\Langs::getLangsWithTitle();
?>

<div class="CatalogMenu" style="display: none;">
    <div class="langMobWrp">
        <div class="langMob">
            <span class="caption">{{$langs[app()->getLocale()]}} <span class="ic-chewrondown"></span></span>
            <div class="expand">
                @foreach($langs as $key => $l)
                    @if($key !== app()->getLocale())
                        <a href="{{route('setlocale',['lang' => $key])}}">{{$l}}</a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    @foreach($catalogMenu as $column)
        <div class="col">
            @foreach($column['children'] as $block)
                <div class="g">
                    @foreach($block['children'] as $item)
                        <?php
                        if($item['type'] == \App\Models\Menu::TYPE_ARBITRARY){
                            $name = $item['name'];

                            foreach ($item['translations'] as $trans){
                                if($trans['lang'] === $currentLang){
                                    $name = $trans['name'];
                                    $url = $trans['url'];
                                }
                            }
                        } else {
                            $modelRel = $item[\App\Models\Menu::getTypesModel()[$item['type']]['rel']];

                            if(is_null($modelRel)){
                                continue;
                            }

                            if($item['type'] == \App\Models\Menu::TYPE_PRODUCT_CATEGORY){
                                $url = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['path'];
                            } elseif($item['type'] == \App\Models\Menu::TYPE_ARBITRARY) {
                                $url = 'javascript:void(0)';
                            } else {
                                $url = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['slug'];
                            }

//                            $url = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['path'];

                            if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                                $url = '/' . $currentLang . $url;
                            }

                            $name = $modelRel[\App\Models\Menu::getTypesModel()[$item['type']]['name']];

                            foreach ($item['translations'] as $trans){
                                if($trans['lang'] === $currentLang){
                                    $name = $trans['name'];
                                }
                            }
                        }
                        ?>
                        <a href="{{$url}}" class="caption">{{$name}}</a>
                        @foreach($item['children'] as $item2)
                            <?php
                            if($item2['type'] == \App\Models\Menu::TYPE_ARBITRARY){
                                $name = $item2['name'];

                                foreach ($item2['translations'] as $trans){
                                    if($trans['lang'] === $currentLang){
                                        $name = $trans['name'];
                                        $url = $trans['url'];
                                    }
                                }
                            } else {
                                try {
                                       $modelRel = $item2[\App\Models\Menu::getTypesModel()[$item2['type']]['rel']];

                                    if(is_null($modelRel)){
                                        continue;
                                    }

                                        if($item2['type'] == \App\Models\Menu::TYPE_PRODUCT_CATEGORY){
                                            $url = \App\Models\Menu::getTypesModel()[$item2['type']]['url_prefix'] . $modelRel['path'];
                                        } elseif($item2['type'] == \App\Models\Menu::TYPE_ARBITRARY) {
                                            $url = 'javascript:void(0)';
                                        } else {
                                            $url = \App\Models\Menu::getTypesModel()[$item2['type']]['url_prefix'] . $modelRel['slug'];
                                        }


                                        if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                                            $url = '/' . $currentLang . $url;
                                        }

                                        $name = $modelRel[\App\Models\Menu::getTypesModel()[$item2['type']]['name']];

                                        foreach ($item2['translations'] as $trans){
                                            if($trans['lang'] === $currentLang){
                                                $name = $trans['name'];
                                            }
                                        } 
                                } catch(Exception $e){
                                    Log::error("menu error",[$item2]);
                                }
                        
                            }
                            ?>

                            <a href="{{$url}}">{{$name}}</a>
                        @endforeach
                    @endforeach
                </div>
            @endforeach
        </div>
    @endforeach

    <div class="col mobile-service-menu" style="display: none;">
        <div class="g">
            @foreach($servicesMenu as $item)
                <?php
                if($item['type'] == \App\Models\Menu::TYPE_ARBITRARY){
                    $name = $item['name'];

                    foreach ($item['translations'] as $trans){
                        if($trans['lang'] === $currentLang){
                            $name = $trans['name'];
                            $url = $trans['url'];
                        }
                    }
                } else {
                    $modelRel = $item[\App\Models\Menu::getTypesModel()[$item['type']]['rel']];

                    if(is_null($modelRel)){
                        continue;
                    }

                    if($item['type'] == \App\Models\Menu::TYPE_PRODUCT_CATEGORY){
                        $url = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['path'];
                    } elseif($item['type'] == \App\Models\Menu::TYPE_ARBITRARY) {
                        $url = 'javascript:void(0)';
                    } else {
                        $url = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['slug'];
                    }

                    //                            $url = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['path'];

                    if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                        $url = '/' . $currentLang . $url;
                    }

                    $name = $modelRel[\App\Models\Menu::getTypesModel()[$item['type']]['name']];

                    foreach ($item['translations'] as $trans){
                        if($trans['lang'] === $currentLang){
                            $name = $trans['name'];
                        }
                    }
                }
                ?>
                <a href="{{$url}}" class="caption">{{$name}}</a>
            @endforeach
        </div>
    </div>

    <div class="col mobcol">
        @foreach($infoMenu as $item)
            <?php
            if($item['type'] == \App\Models\Menu::TYPE_ARBITRARY){
                $name = $item['name'];

                foreach ($item['translations'] as $trans){
                    if($trans['lang'] === $currentLang){
                        $name = $trans['name'];
                        $url = $trans['url'];
                    }
                }
            } else {
                $modelRel = $item[\App\Models\Menu::getTypesModel()[$item['type']]['rel']];

                if(is_null($modelRel)){
                    continue;
                }

                $url = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['slug'];

                if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                    $url = '/' . $currentLang . '/' . $url;
                } else {
                    $url =  '/' . $url;
                }

                $name = $modelRel[\App\Models\Menu::getTypesModel()[$item['type']]['name']];

                foreach ($item['translations'] as $trans){
                    if($trans['lang'] === $currentLang){
                        $name = $trans['name'];
                    }
                }
            }
            ?>
            <div class="g">
                <a href="{{$url}}" class="caption">{{$name}}</a>
            </div>
        @endforeach
    </div>
    <div class="menuFooter">
        <?php $phones = json_decode(app(Setting::class)->get('phones'),true);?>
        <div class="t1">{{app(Setting::class)->get('contact_center') ?? ''}}</div>
        @isset($phones[1]['number'])
            <a href="tel:{{$phones[1]['number']}}">{{$phones[1]['number']}}</a>
        @endisset
    </div>
</div>
