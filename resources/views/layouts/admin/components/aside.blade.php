<?php

use Illuminate\Support\Facades\Request;

?>

<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="pt-4">
                <li class="sidebar-item @if(in_array(Request::segment(1),['admin']) && Request::segment(2) == '') active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin')}}" aria-expanded="false">
                        <i class="mdi mdi-blur-linear"></i>
                        <span class="hide-menu">Панель приборов</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['users'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('users.index')}}" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">Пользователи</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['pages'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('pages.index')}}" aria-expanded="false">
                        <i class="mdi mdi-book-open-page-variant"></i>
                        <span class="hide-menu">Страницы</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['services'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('services.index')}}" aria-expanded="false">
                        <i class="mdi mdi-drawing-box"></i>
                        <span class="hide-menu">Услуги</span>
                    </a>
                </li>


                <li class="sidebar-item @if(in_array(Request::segment(2),['attributes'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('attributes.index')}}" aria-expanded="false">
                        <i class="mdi mdi-filter"></i>
                        <span class="hide-menu">Атрибуты</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['category'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('category.index')}}" aria-expanded="false">
                        <i class="mdi mdi-format-list-bulleted"></i>
                        <span class="hide-menu">Категории</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['products'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('products.index')}}" aria-expanded="false">
                        <i class="mdi mdi-widgets"></i>
                        <span class="hide-menu">Продукты</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['orders'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('orders.index')}}" aria-expanded="false">
                        <i class="mdi mdi-cart"></i>
                        <span class="hide-menu">Заказы</span>
                    </a>
                </li>


                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-newspaper"></i>
                        <span class="hide-menu">Блог</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item @if(in_array(Request::segment(3),['articles'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('articles.index')}}" aria-expanded="false">
                                <i class="mdi mdi-message-outline"></i>
                                <span class="hide-menu">Публикации</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(3),['tags'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('tags.index')}}" aria-expanded="false">
                                <i class="mdi mdi-tag"></i>
                                <span class="hide-menu">Теги</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(3),['subscribe'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('subscribe.index')}}" aria-expanded="false">
                                <i class="mdi mdi-email"></i>
                                <span class="hide-menu">Подписки</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['faq'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('faq.index')}}" aria-expanded="false">
                        <i class="mdi mdi-forum"></i>
                        <span class="hide-menu">FAQ</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['users-admin'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('users-admin.index')}}" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">Аадминистраторы</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-format-paint"></i>
                        <span class="hide-menu">Внешний вид </span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item @if(in_array(Request::segment(2),['widgets'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/widgets" aria-expanded="false">
                                <i class="mdi mdi-widgets"></i>
                                <span class="hide-menu">Виджеты</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(2),['menu'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('menu.index')}}" aria-expanded="false">
                                <i class="mdi mdi-menu"></i>
                                <span class="hide-menu">Меню</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-settings"></i>
                        <span class="hide-menu">Настройки</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item @if(in_array(Request::segment(4),['main'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/main" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Главные</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(4),['contacts'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/contacts" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Контакты</span>
                            </a>
                        </li>
                        <li class="sidebar-item @if(in_array(Request::segment(4),['blog'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/blog" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Блог</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
