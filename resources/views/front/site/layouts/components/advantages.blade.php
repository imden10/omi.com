@if(isset($content['list']) && count($content['list']))
    <div class="advantageInfoBlock">
        @foreach($content['list'] as $item)
            <div class="item">
                <span class="topTotle">{{$item['title']}}</span>
                <span class="lighter">{{$item['text']}}</span>
            </div>
        @endforeach
    </div>
@endif
