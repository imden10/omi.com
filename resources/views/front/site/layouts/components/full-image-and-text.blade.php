<div class="imgAndTitle">
    @if(! empty($content['title']))
        <div class="imgtitle">{{$content['title']}}</div>
    @endif
    <img src="{{get_image_uri($content['image'], 'original')}}" alt="">
</div>
