@if (!empty($content['title']))
    <h3>{{ $content['title'] }}</h3>
@endif

<div class="twoColText">
    <div>{!! $content['text1'] !!}</div>
    <div>{!! $content['text2'] !!}</div>
</div>


