@if(isset($content['list']) && count($content['list']))
<div class="advantageInfoBlock">
    @foreach($content['list'] as $item)
        <div class="item">
            <span class="topTotle"><span class="spincrement">{{$item['number']}}</span> {{$item['unit'] ?? ''}} </span>
            <span class="bolder">{{$item['text']}}</span>
        </div>
    @endforeach
</div>
@endif
