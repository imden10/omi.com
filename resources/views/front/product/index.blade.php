@extends('layouts.app')

@section('content')
    <section class="productSingleSection">
        <div class="container">
            <div class="breadCrumbs">
                <a href="{{route('main')}}">{{__('main')}}</a>
                @foreach($breadcrumbs as $item)
                    @if(! $loop->last)
                        <a href="{{$item['href']}}">{{$item['title']}}</a>
                    @else
                        <a>{{$item['title']}}</a>
                    @endif
                @endforeach
            </div>
            <div class="productTwoCol">
                <div>
                    <h1 class="productTitle">{{$prod_trans->name}}</h1>
                    <div class="articleDiv">
                        {!! $prod_trans->description !!}
                    </div>
                </div>
                <div>
                    <div class="productImgSize">
                        <img class="prodMainImg" src="{{get_image_uri($product->image,'original')}}" alt="">
                        <div class="priceWrp">
                            <div>
                                @foreach($product->prices as $price)
                                    @if($loop->last && $price->retail_count_unit_id)
                                        <div class="priceTxt priceTxt-retail">
                                            <div class="txt">{{__("retail up to")}} {{$price->retail_count}} {{\App\Models\ProductPrices::getUnits()[$price->retail_count_unit_id]}}</div>
                                            <span><span class="price-retail">{{$price->retail_price}}</span>{{__('uah')}}</span>
                                        </div>
                                        @if($price->wholesale_price > 0 && $price->wholesale_count_unit_id)
                                        <div class="priceTxt priceTxt-wholesale">
                                            <div class="txt">{{__("wholesale from")}} {{$price->wholesale_count}} {{\App\Models\ProductPrices::getUnits()[$price->wholesale_count_unit_id]}}</div>
                                            <span><span class="price-wholesale">{{$price->wholesale_price}}</span>{{__('uah')}}</span>
                                        </div>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                            <div>
                                <div class="zakazWrp">
                                    <div class="litersBtns">
                                        @foreach($product->prices as $price)
                                        	@if($price->retail_count_unit_id && $price->wholesale_count_unit_id && $price->count_position_unit_id)
                                            <button
                                                data-price-retail="{{$price->retail_price}}" data-text-retail="{{__("retail up to")}} {{$price->retail_count}} {{\App\Models\ProductPrices::getUnits()[$price->retail_count_unit_id]}}"
                                                data-price-wholesale="{{$price->wholesale_price}}" data-text-wholesale="{{__("wholesale from")}} {{$price->wholesale_count}} {{\App\Models\ProductPrices::getUnits()[$price->wholesale_count_unit_id]}}"
                                                @if($loop->last) class="active" @endif
                                            >
                                                {{$price->count_position}}{{\App\Models\ProductPrices::getUnits()[$price->count_position_unit_id]}}
                                            </button>
                                            @endif
                                        @endforeach
                                    </div>
                                    <button class="buy buy-btn">{{__('Checkout2')}}</button>
                                </div>
                            </div>
                        </div>
                        @if($product->is_stock_widget)
                            {!! Widget::show(109) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(app()->getLocale() === 'ru')
        {!! Widget::show(96) !!}
    @elseif(app()->getLocale() === 'en')
        {!! Widget::show(97) !!}
    @endif

    @include('front.site.layouts.includes.constructor', ['constructor' => $product->getTranslation(app()->getLocale())->constructor->data, 'catalog' => 'pages::site.components.'])

    <?php
        $recommendationIds = [];
        $productsRec = null;
        if($product->recommendations){
            $recommendationIds = json_decode($product->recommendations,true);
            $productsRec = \App\Models\Products::query()->active()->whereIn('id',$recommendationIds)->get();
        }
    ?>

    @if($productsRec)
        <section class="recomendSection">
            <div class="container">
                <h2>Рекомендации</h2>
                <p class="subtext">Мы подобрали для вас рекомендуемые товары, которые пользуются популярностью</p>
                <div class="relatedProds">
                    @foreach($productsRec as $productRec)
                        @if(isset($productRec->categories[0]->path))
                            <div class="item">
                                <a href="{{$productRec->categories[0]->path}}/{{$productRec->slug}}">
                                    <div class="imgWrp">
                                        <img src="{{get_image_uri($productRec->image)}}" alt="">
                                    </div>
                                    <span class="name">{{$productRec->getTranslation(app()->getLocale())->name}}</span>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @endif


    <div class="modalWrap modalWrap-request-form" style="display: none">
        <div class="modalBody">
            <div class="close close-request-modal-btn"></div>
            <div class="caption">{{__('Checkout')}}</div>
            <div class="text">{{__('Fill the form')}}</div>
            <form action="#" id="add_request_form" novalidate="novalidate">
                @csrf

                <div class="inputWrp">
                    <input type="text" name="name" placeholder="{{__('name')}}">
                </div>

                <div class="inputWrp">
                    <input type="text" name="phone" placeholder="{{__('phone number')}}">
                </div>

                <div class="inputWrp">
                    <input type="text" name="email" {{--class="error"--}} placeholder="{{__('email')}}">
                </div>

                <input type="hidden" name="product_id" value="{{$product->id}}">

                <button type="submit">{{__('Send data')}}</button>

                <input type="hidden" name="no_recaptcha" value="1">

                <div class="g-recaptcha"
                     data-sitekey="{{env('GOOGLE_RECAPTCHA_SITE_KEY')}}"
                     data-size="invisible">
                </div>
            </form>
        </div>
    </div>
@endsection

@push('styles')

@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            // prod ingle liters pick
            $('.zakazWrp .litersBtns button').on('click', function(){
                $('.zakazWrp .litersBtns button').removeClass('active');
                $(this).addClass('active');
            });

            $(".litersBtns button").on('click',function(){
                $(".priceWrp .priceTxt-retail .txt").text($(this).data('text-retail'));
                $(".priceWrp .priceTxt-wholesale .txt").text($(this).data('text-wholesale'));

                $(".priceWrp .priceTxt-retail .price-retail").text($(this).data('price-retail'));
                $(".priceWrp .priceTxt-wholesale .price-wholesale").text($(this).data('price-wholesale'));

            });

            $(".close-request-modal-btn").on('click',function () {
                $(this).closest('.modalWrap').hide();
            });

            $(".buy-btn").on('click', function () {
                $(".modalWrap-request-form").show();
            });

            $("#add_request_form").validate({
                rules: {
                    'email': {
                        required: true,
                        email: true
                    },
                    'name': {
                        required: true,
                        minlength: 2
                    },
                    'phone': {
                        required: true,
                        minlength: 17
                    }
                },
                messages: {
                    phone: "{{__('Enter your phone number')}}",
                    name: "{{__('Enter your name')}}",
                    email: "{{__('Enter your email')}}",
                },
                submitHandler: function (form) {
                    grecaptcha.execute().then(function(){
                        $.ajax({
                            type: "POST",
                            url: "{{route('product.add-request')}}",
                            data: $("#add_request_form").serialize(),
                            beforeSend: function (jqXHR, settings) {

                            },
                            success: function (data) {
                                if(data.success) {
                                    $('form input[type="text"]').val('');
                                    $('#add_request_form').closest('.modalWrap-request-form').hide();
                                    $('.modalWrapFreeConsultation').show();
                                    setTimeout(function() {
                                        $('.modalWrapFreeConsultation').hide();
                                    }, 3000);
                                }
                            },
                            error: function (jqXHR, text, error) {
                                console.log('Get error!');
                            }
                        });
                        return false;
                    });
                }
            });
        });
    </script>
@endpush
