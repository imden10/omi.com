@extends('layouts.app')

@section('content')
    <?php $currentLang = app()->getLocale(); ?>
    <section class="blogMainSection">
        <div class="container">
            <h1>{{__('omi blog')}}</h1>

            {!! Widget::show(110) !!}
{{--            {!! Widget::show(112) !!}--}}

            <div class="blogCategorys">
                <div class="categoryLnkWrp">
                    <div class="switcher">{{__('Show category list')}}</div>
                    <div class="hideWrp">
                        @foreach(\App\Models\BlogTags::query()->get() as $tag)
                            <?php
                                $url = "/blog/tag/" . $tag->slug;
                                if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                                    $url = '/' . $currentLang  . $url;
                                }
                            ?>
                            <a href="{{$url}}" @if(isset($currentSlug) && $currentSlug == $tag->slug) class="active" @endif>{{$tag->getTranslation(app()->getLocale())->name}}</a>
                        @endforeach
                    </div>
                </div>
                <div class="blogPagesLnkWrp">
                    @foreach($model as $item)
                    <div class="itemWrp">
                        <?php
                            $url = "/blog/" . $item->slug;
                            if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                                $url = '/' . $currentLang  . $url;
                            }
                        ?>
                        <a href="{{$url}}">
                            <img src="{{get_image_uri($item->image_thumb, 'original')}}" alt="">
                            <div class="top">
                                <div></div>
                                <div>{{\Carbon\Carbon::parse($item->public_date)->format('d.m.y')}}</div>
                            </div>
                            <div class="bot">
                                {{$item->getTranslation(app()->getLocale())->name}}
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @if(\Illuminate\Support\Facades\Session::has('success') && \Illuminate\Support\Facades\Session::get('success') === 'subscribe')
        <div class="modalWrap">
            <div class="modalBody">
                <div class="close close-modal-btn"></div>
                <img src="/img/modalicon.svg" alt="" class="modalIcon">
                <div class="caption">{{__('Thanks for subscribing')}}</div>
                <a href="#" class="backLnk close-modal-btn">{{__('Back to site')}}</a>
            </div>
        </div>
    @endif
@endsection

@push('scripts')
    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_RECAPTCHA_SITE_KEY')}}"></script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.2/flickity.pkgd.min.js" integrity="sha512-cA8gcgtYJ+JYqUe+j2JXl6J3jbamcMQfPe0JOmQGDescd+zqXwwgneDzniOd3k8PcO7EtTW6jA7L4Bhx03SXoA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
    <script>
        function validateEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        function onSubmit(token)
        {
            let email = $('#subscribe_form input[type="email"]').val();
            if(! validateEmail(email)){
                $(".error-email-subs").show();
            } else {
                document.getElementById("subscribe_form").submit();
            }
        }

        $(document).ready(function () {
            // showCaseSlider JS start
            var recomendItemSlides = new Flickity( '.blogSlider', {
                prevNextButtons: false,
                pageDots: false
            });
            $('.blogSlider .backbtn').on('click', function(){
                recomendItemSlides.previous( true, false );
            });
            $('.blogSlider .forwardbtn').on('click', function(){
                recomendItemSlides.next( true, false );
            });
            $('.categoryLnkWrp .switcher').on('click', function(){
                $('.categoryLnkWrp .hideWrp').toggleClass('show');
                $('.categoryLnkWrp').toggleClass('active');
            });
        });

    </script>
@endpush
