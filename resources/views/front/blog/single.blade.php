@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$page->meta_description}}">
    <meta name="keywords" content="{{$page->meta_keywords}}">
    <meta property="og:locale" content="uk_UA" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{$page->name}}" />
    <meta property="og:description" content="{{strip_tags($page->excerpt)}}" />
    <meta property="og:url" content="{{\Illuminate\Support\Facades\URL::current()}}" />
    <meta property="og:site_name" content="{{env('APP_NAME')}}" />
    <meta property="article:published_time" content="{{\Carbon\Carbon::now()->toIso8601String()}}" />
    <meta property="og:image" content="{{get_image_uri($page->image, 'original')}}" />
    <?php [$imgWidth, $imgHeight] = getimagesize(get_image_uri($page->image, 'original'));?>
    <meta property="og:image:width" content="{{$imgWidth}}" />
    <meta property="og:image:height" content="{{$imgHeight}}" />
    <meta name="twitter:card" content="summary_large_image" />
@endsection

@section('content')
    <section class="blogPostSingle">
        <div class="container">
            <div class="headBlogPost">
                <div>
                    <a href="{{route('blog')}}" class="back"><span class="ic-left"></span> {{__('back')}}</a>
                    <a href="#" class="soc"><span class="ic-faceb"></span></a>
                    <a href="#" class="soc"><span class="ic-wtitter"></span></a>
                    <a href="#" class="soc"><span class="ic-link"></span></a>
                </div>
                <div>
                    <span class="date">{{\Carbon\Carbon::parse($page->public_date)->format('d.m.y')}}</span>
                </div>
            </div>
        </div>
        <div class="imgBannerAndText">
            <img src="{{get_image_uri($page->image,'original')}}" alt="">
            <div class="txtWrp">
                <h1>{{$page->getTranslation(app()->getLocale())->name}}</h1>
            </div>
        </div>
        <div class="articleDiv">
            @include('front.site.layouts.includes.constructor', ['constructor' => $page->getTranslation(app()->getLocale())->constructor->data, 'catalog' => 'pages::site.components.'])
        </div>
    </section>

    <?php
        $currentLang = app()->getLocale();
        $similar = \App\Models\BlogArticles::query()
            ->where('product_category_id',$page->product_category_id)
            ->where('id','<>',$page->id)
            ->limit(3)
            ->inRandomOrder()
            ->active()
            ->get();
    ?>
    @if(count($similar) && $page->product_category_id)
    <section class="relatedPostsSection">
        <div class="container">
            <div class="showcaseBlock">
                <h2 class="caption">{{__("Read more")}}</h2>
                <div class="itemsWrp">
                    @foreach($similar as $item)
                        <?php
                        $url = "/blog/" . $item->slug;
                        if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                            $url = '/' . $currentLang  . $url;
                        }
                        ?>
                        <div class="itemWrp">
                            <a href="{{$url}}">
                                <img src="{{get_image_uri($item->image_thumb, 'original')}}" alt="">
                                <div class="top"></div>
                                <div class="bot">
                                    {{$item->getTranslation(app()->getLocale())->name}}
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @endif
@endsection

@push('styles')
    <style>
        .post-linked-in{
            position: relative;
        }
        .copy-link{
            -webkit-transition: .5s;
            -o-transition: .5s;
            transition: .5s;
            opacity: 0;
            position: absolute;
            top: 50px;
            left: -150px;
            background-color: rgba(0,0,0,.7);
            color: #fff;
            padding: 10px;
            border-radius: 5px;
            font-size: 14px;
        }
        .visible{
            opacity: 1;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function(){
            $(document).on('click', '.post-linked-in', function(e) {
                e.preventDefault();

                const postUrl = $(this).attr('data-url');
                const input = document.createElement("input");

                input.value = postUrl;
                document.body.appendChild(input);
                input.select();

                try {
                    document.execCommand('copy');
                    $('.copy-link').addClass('visible');
                    function hideAlert() {
                        $('.copy-link').removeClass('visible');
                    }
                    setTimeout(hideAlert, 2000)
                }
                catch (err) {
                    console.log('Oops, unable to copy');
                }

                document.body.removeChild(input);
            });
        });
    </script>
@endpush
