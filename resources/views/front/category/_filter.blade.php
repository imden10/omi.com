<div class="filterAndButton">
    <button class="filterSwitch">{{__('filter')}}</button>
    <form method="get" id="form_filter">
    <div class="filter">
        @if(count($filters) && count($variants))
            @foreach($filters as $key => $filter)
                @if(isset($variants[$key]))
                    <div class="groupItems">
                        <div class="caption">{{$filter['name']}} <span class="count">({{$variants[$key]['count']}})</span> <span class="ic-chewrondown btn-min-filter"></span></div>
                        <div class="g">
                            <?php $i = 0;?>
                            @foreach($variants[$key]['variants'] as $slug => $variant)
                                <?php $i++;?>
                                    <div class="item @if($i > 5) item-hide @endif">
                                        <div class="decoWrp">
                                            <input type="checkbox" @if($variant['active']) checked @endif id="variant_{{$key}}_{{$slug}}" name="filter[{{$key}}][]" value="{{$slug}}" onchange="$('#form_filter').submit()">
                                            <div class="deco">
                                                <span class="ic-check"></span>
                                            </div>
                                        </div>
                                        <label for="variant_{{$key}}_{{$slug}}" class="name">{{$variant['name']}} ({{$variant['count']}})</label>
                                    </div>
                            @endforeach

                            @if(count($variants[$key]['variants']) > 5)
                                <span class="see-more-items">{{__('Show more')}}</span>
                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
    </form>
</div>

@push('styles')
    <style>
        .groupItems .item.item-hide {
            display: none !important;
        }

        .see-more-items {
            color: #3986BA;
            margin-top: 15px;
            display: inline-block;
            text-decoration: underline;
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            $(".see-more-items").on('click',function () {
                $(this).closest('.g').find('.item-hide').removeClass('item-hide');
                $(this).remove();
            });
        })
    </script>
@endpush
