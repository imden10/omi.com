<div class="item">
    <a href="{{$lang_prefix}}/{{$model->path}}/{{$product->slug}}">
        <div class="imgWrp">
            <img src="{{get_image_uri($product->image,'original')}}" alt="">
        </div>
        <span class="name">{{$product->name}}</span>
    </a>
</div>
