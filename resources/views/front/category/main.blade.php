@extends('layouts.app')

@section('content')
    <section class="categorySection">
        <div class="container">
            <h1>{{$cat_info->title}}</h1>
            <div class="categoryDetailText">
                <div>
                    {!! $cat_info->description !!}
                </div>
                <?php
                $options = $cat_info->options ? json_decode($cat_info->options,true) : [];
                ?>
                @if(count($options))
                    <div>
                        <div class="categoryAdvantages">
                            @foreach($options as $option)
                                <div class="item"><p>{{$option}}</p></div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
            @if(count($children))
                <div class="categoryLinks">
                    <?php
                        $lang_prefix = '';
                        if(app()->getLocale() !== \App\Models\Langs::getDefaultLangCode()){
                            $lang_prefix = '/' . app()->getLocale();
                        }
                    ?>
                    @foreach($children as $child)
                        <div class="itemWrp">
                            <a href="{{$lang_prefix}}/catalog/{{$child->path}}">
                                <img src="{{get_image_uri($child->image,'original')}}" alt="">
                                <div class="top">
                                    <?php
                                        $countSubCategories = $child->countSubCategories();
                                        $countProduct = $child->countProductsInSubCategories();
                                    ?>
                                    <span>{{$countSubCategories}} {{ trans_choice('category_choice',$countSubCategories )}}</span>
                                    <span>{{$countProduct}} {{ trans_choice('product_choice',$countProduct )}}</span>
                                </div>
                                <div class="bot">{{$child->title}}</div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endif
{{--            <div class="seoBlock">--}}
{{--                <div class="articleDiv">--}}
{{--                    <h2>Заголовок перед текстои для СЕО</h2>--}}
{{--                    <p>Розетка также может направлять вам информационные и маркетинговые рассылки (новости, акции компании, информацию об акциях, промокоды и скидки, персональные рекомендации, персональные скидки и предложения), которые содержат информацию о товарах и/или услугах, рекламные и коммерческие предложения по таким товарам и/или услугам.</p>--}}
{{--                    <a href="#" class="showSeoTxt">Показать весь текст</a>--}}
{{--                    <div class="seoHidden" style="display: none;">--}}
{{--                        <p>Розетка также может направлять вам информационные и маркетинговые рассылки (новости, акции компании, информацию об акциях, промокоды и скидки, персональные рекомендации, персональные скидки и предложения), которые содержат информацию о товарах и/или услугах, рекламные и коммерческие предложения по таким товарам и/или услугам.</p>--}}
{{--                        <p>Розетка также может направлять вам информационные и маркетинговые рассылки (новости, акции компании, информацию об акциях, промокоды и скидки, персональные рекомендации, персональные скидки и предложения), которые содержат информацию о товарах и/или услугах, рекламные и коммерческие предложения по таким товарам и/или услугам.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </section>

    @if(app()->getLocale() === 'ru')
        {!! Widget::show(96) !!}
        {!! Widget::show(102) !!}
    @elseif(app()->getLocale() === 'en')
        {!! Widget::show(97) !!}
        {!! Widget::show(103) !!}
    @endif
@endsection

@push('scripts')

@endpush
