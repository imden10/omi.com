<?php
$selVariants = request()->get('filter',[]);
?>

@if(count($selVariants))
    <div class="topZone">
        <p class="selectedInfo">{{__('selected')}} <span>{{$countProducts}} {{ trans_choice('product2_choice',$countProducts )}}</span></p>
        <!-- <div class="filteringLnk">Самые популярные <span class="ic-chewrondown"></span></div> -->
    </div>

    <div class="filterTags">
        <div class="item reset" data-url="{{request()->url()}}">{{__('filters reset')}}</div>
        @foreach($selVariants as $attr => $sel)
            @foreach($sel as $slug)
                @foreach($variants as $var)
                    @if(isset($var['variants'][$slug]))
                        <div class="item">
                            <span class="name">{{$var['variants'][$slug]['name']}}</span>
                            <span class="ic-cross btn-clear-filter" data-attr="{{$attr}}" data-slug="{{$slug}}"></span>
                        </div>
                    @endif
                @endforeach
            @endforeach
        @endforeach
    </div>
@endif

@push('scripts')
    <script>
        $(".filterTags .item.reset").on('click',function(){
            let url = $(this).data('url');
            document.location.href = url;
        });

        $(".filterTags .btn-clear-filter").on('click',function () {
            $(".filter input[id='variant_" + $(this).data('attr') + "_" + $(this).data('slug') + "']").prop('checked',false);
            $('#form_filter').submit();
        });
    </script>
@endpush
