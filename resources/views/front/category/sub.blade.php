@extends('layouts.app')

@section('content')
    <?php
        $lang_prefix = '';
        if(app()->getLocale() !== \App\Models\Langs::getDefaultLangCode()){
            $lang_prefix = '/' . app()->getLocale();
        }
    ?>

    <section class="productsSection">
        <div class="container">
            <div class="breadCrumbs">
                <a href="{{route('main')}}">{{__('main')}}</a>
                @foreach($breadcrumbs as $item)
                    @if(! $loop->last)
                        <a href="{{$item['href']}}">{{$item['title']}}</a>
                    @else
                        <a>{{$item['title']}}</a>
                    @endif
                @endforeach
            </div>
            <div class="caterogyBranches">
                <h1>{{$cat_info->title}}</h1>
                <div class="branchesWrp">
                    @if(count($children))
                        @foreach($children as $child)
                            <div class="awrap">
                                <a href="{{$child->path}}">
                                    <img src="{{get_image_uri($child->image,'original')}}" alt="">
                                    <span>{{$child->title}}</span>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="productAndFilter">
                <div class="filterWrp">
                    @include("front.category._filter")
                </div>
                <div class="prodsWrp">
                    @include("front.category._filter_tags")
                    <div class="prodItemsWrp">
                        @if(count($products))
                            @foreach($products as $product)
                                @include("front.category._product_item",['product' => $product])
                            @endforeach
                        @endif
                    </div>
                    {{ $products->appends(request()->input())->links('layouts._pagination',['paginator' => $products]) }}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-min-filter').on('click',function () {
                $(this).closest('.groupItems').toggleClass('minimize');
            })

            $('.filterSwitch').on('click',function () {
                $(this).closest('.filterAndButton').find('.filter').toggleClass('show');
            })
        })
    </script>
@endpush
