@extends('layouts.app')

@section('content')
    <section class="serviceSingle">
        <div class="singleHead" style="background-image: url({{get_image_uri($page->image,'original')}})">
            <div class="container">
                <div class="titleWrp">
                    <h1>{{$page->getTranslation(app()->getLocale())->name}}</h1>
                    <p class="subtxt">{{$page->getTranslation(app()->getLocale())->excerpt}}</p>
                    <a href="#" class="titleLnk">{{__('order consultation')}}</a>
                </div>
            </div>
        </div>
        <div class="container">
            <h2>{{$page->getTranslation(app()->getLocale())->name}}</h2>
            <div class="categoryDetailText">
                <div>
                    @if($page->getTranslation(app()->getLocale())->description !== '<p><br></p>')
                        {!! $page->getTranslation(app()->getLocale())->description !!}
                    @endif
                </div>
                    <?php
                        $options = $page->getTranslation(app()->getLocale())->options ? json_decode($page->getTranslation(app()->getLocale())->options,true) : [];
                    ?>
                    @if(count($options))
                        <div>
                            <div class="categoryAdvantages">
                                @foreach($options as $option)
                                    <div class="item"><p>{{$option}}</p></div>
                                @endforeach
                            </div>
                        </div>
                    @endif
            </div>
            <div class="articleDiv">
                @include('front.site.layouts.includes.constructor', ['constructor' => $page->getTranslation(app()->getLocale())->constructor->data, 'catalog' => 'pages::site.components.'])
            </div>
        </div>
    </section>

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.0/masonry.pkgd.min.js"></script>
    <script src="{{asset('js/jquery.spincrement.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.masonryWrap').masonry({
                // options
                itemSelector: '.itemWraper'
            });
            setTimeout(() => {
                $('.masonryWrap').masonry();
            }, 2000);


            var show = true;
            var countbox = ".advantageInfoBlock";
            $(window).on("scroll load resize", function () {
                if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
                var w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
                var e_top = $(countbox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
                var w_height = $(window).height(); // Высота окна браузера
                var d_height = $(document).height(); // Высота всего документа
                var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
                if (w_top + 500 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
                    $('.spincrement').css('opacity', '1');
                    $('.spincrement').spincrement({
                        thousandSeparator: " ",
                        duration: 1200
                    });

                    show = false;
                }
            });
        });
    </script>
@endpush
