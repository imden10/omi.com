@if($content['image_position'] == 'left')
<div class="leftImg">
    <div>
        <img src="{{get_image_uri($content['image'], 'original')}}" alt="">
    </div>
    <div>
        @if(! empty($content['title']))
            <h3>{{$content['title']}}</h3>
        @endif
       {!! $content['description'] !!}
    </div>
</div>
@elseif($content['image_position'] == 'right')
<div class="rightImg">
    <div>
        @if(! empty($content['title']))
            <h3>{{$content['title']}}</h3>
        @endif
        {!! $content['description'] !!}
    </div>
    <div>
        <img src="{{get_image_uri($content['image'], 'original')}}" alt="">
    </div>
</div>
@endif
