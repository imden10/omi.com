@if(! empty($content['title']))
    {!! $content['title'] !!}
@endif

@if($content['type'] == 'ul')
<ul>
    @foreach($content['list'] as $item)
        <li><p>{!! $item['item'] !!}</p></li>
    @endforeach
</ul>
@elseif($content['type'] == 'ol')
<ol>
    @foreach($content['list'] as $item)
        <li><p>{!! $item['item'] !!}</p></li>
    @endforeach
</ol>
@endif
