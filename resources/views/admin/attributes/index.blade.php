@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Атрибуты</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('attributes.create')}}" class="btn btn-primary float-right">
                    <i class="fa fa-plus"></i>
                    Добавить
                </a>
            </div>
            <div class="card-body">

                @if(count($model))
                <form action="" method="get">
                    <div class="form-row">

                        <div class="form-group col-md-2">
                            <label>Поиск</label>
                            <input type="text" class="form-control" name="search" value="{{old('search', request()->input('search'))}}">
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Искать</button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('attributes.index') }}" class="btn btn-danger form-control text-white">Сбросить</a>
                        </div>
                    </div>
                </form>

                <div class="form-row">
                    <div class="form-group col-md-2">
                        <span class="btn btn-danger btn-xs text-white btn-delete-all disabled">
                            Удалить выделенные
                        </span>
                    </div>
                </div>
                @endif

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 50px">
                                <input type="checkbox" class="check-all">
                            </th>
                            <th style="width: 50px">ID</th>
                            <th>Название</th>
                            <th style="width: 100px">Сортировка</th>
                            <th style="width: 200px">Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr data-id="{{$item->id}}">
                                <td>
                                    <input type="checkbox" data-check="{{$item->id}}" name="check[]" value="{{$item->id}}">
                                </td>
                                <td>{{$item->id}}</td>
                                <td>
                                    <a href="{{ route('attributes.edit', $item->id) }}">
                                        {{$item->name}}
                                    </a>
                                </td>
                                <td>
                                    <form action="{{route('attributes.sort')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$item->id}}">
                                        <input type="text" class="form-control" name="order" value="{{$item->order}}">
                                    </form>
                                </td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('attributes.destroy', $item->id) }}" method="POST">

                                                <a href="{{ route('attributes.edit', $item->id) }}" class="btn btn-xs btn-primary" style="width: 30px">
                                                    <i class="fas fa-edit fa-lg"></i>
                                                </a>

                                                @csrf
                                                @method('DELETE')

                                                <a href="javascript:void(0)" title="Удалить"  style="width: 30px" class="btn btn-danger btn-xs delete-item-btn text-white">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <form action="{{route('attributes.delete-selected')}}" method="post" id="delete_sel_form">
                    @csrf
                    <input type="hidden" name="ids">
                </form>
                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                let _this = $(this);
                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить запись?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        _this.closest('form').submit();
                    }
                });
            });

            // выделить все
            $("#check_all, .check-all").on( 'click', function() {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length>0 );
            });

            $('#check_all, .check-all, input[type="checkbox"][name*="check"]').on( 'click', function() {
                let checked = [];
                $('tbody input:checkbox').each(function (i,elem) {
                    if ($(this).prop('checked')){
                        checked.push($(this).val());
                    }
                });

                if(checked.length){
                    $(".btn-delete-all").removeClass('disabled');
                } else {
                    $(".btn-delete-all").addClass('disabled');
                }
            });

            $(".btn-delete-all").on('click',function () {

                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить атрибуты!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        let checked = [];
                        $('tbody input:checkbox').each(function (i,elem) {
                            if ($(this).prop('checked')){
                                checked.push($(this).val());
                            }
                        });

                        $('#delete_sel_form').find('input[name="ids"]').val(JSON.stringify(checked));
                        $('#delete_sel_form').submit();
                    }
                });
            })
        });
    </script>
@endpush
