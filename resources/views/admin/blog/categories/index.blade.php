@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item active" aria-current="page">Категорії</li>
        </ol>
        <a href="{{route('categories.create')}}" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Додати
        </a>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body" id="admin_categories">
                <TreeBlogCategories :categories="{{$model}}"></TreeBlogCategories>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('css/admin/menu.css')}}">
    <style>
        nav.breadcrumb-nav {
            position: relative;
        }
        nav.breadcrumb-nav a.btn {
            position: absolute;
            right: 15px;
            top: 4px;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('/js/admin_menu_categories.js')}}"></script>
@endpush

