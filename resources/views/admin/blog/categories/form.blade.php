<?php
use App\Models\Menu;

/* @var $model Menu */
/* @var $errors array */
?>

@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item"><a href="{{route('categories.index')}}">Категорії</a></li>
            @if($model->id)
                <li class="breadcrumb-item active" aria-current="page">Редагувати</li>
            @else
                <li class="breadcrumb-item active" aria-current="page">Додати</li>
            @endif
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if($model->id)
                        <form action="{{route('categories.update', $model->id)}}" method="post" class="form-horizontal">
                        @method('PUT')
                    @else
                        <form action="{{route('categories.store')}}" method="post" class="form-horizontal">
                    @endif
                    @csrf

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Головне</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a>
                        </li>
                    </ul>
                    <br>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                            <div class="form-group row">
                                <label class="col-md-3 text-right" for="name"><span style="color:red">*</span>Заголовок</label>
                                <div class="col-md-9">
                                    <input type="text" name="name" id="name" class="form-control" value="{{$model->name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right" for="name">URL</label>
                                <div class="col-md-9">
                                    <input type="text" name="slug" id="slug" class="form-control" value="{{$model->slug}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right" for="text">Текст</label>
                                <div class="col-md-9">
                                    <textarea name="text" class="summernote editor" cols="30" rows="10">{{$model->text}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right" for="status">
                                    Status</label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="status" value="1" id="status" @if($model->status) checked @endif>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                            <div class="form-group row">
                                <label class="col-md-3 text-right" for="meta_title">Meta Title</label>
                                <div class="col-md-9">
                                    <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{$model->meta_title}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right" for="meta_keywords">Meta Keywords</label>
                                <div class="col-md-9">
                                    <input type="text" name="meta_keywords" id="meta_keywords" class="form-control" value="{{$model->meta_keywords}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right" for="meta_description">Meta Description</label>
                                <div class="col-md-9">
                                    <input type="text" name="meta_description" id="meta_description" class="form-control" value="{{$model->meta_description}}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="submit" class="btn btn-success btn-sm float-right text-white" value="Сохранить">
                </form>
                </div>
            </div>
        </div>
    </div>
@endsection
