@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item active" aria-current="page">Підписка</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card card-exportable">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th data-field="email">E-mail</th>
                        <th data-field="created_at">Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr>
                                <td>{{$item->email}}</td>
                                <td>{{$item->created_at->format('d.m.Y')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
    <script src="{{asset('/myLib/im-export-table-lib.js')}}"></script>
    <script>
        $(document).ready(function(){
            let exportLib = new imExportTableLib({
                cardQuerySelector: ".card-exportable",
                dbTable: "subscribes"
            });
        });
    </script>
@endpush
