@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item"><a href="{{route('articles.index')}}">Публикации</a></li>
            <li class="breadcrumb-item active" aria-current="page">Добавить</li>
        </ol>
    </nav>

    <form class="form-horizontal" method="POST" action="{{ route('articles.store') }}">
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                            <li class="nav-item">
                                <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Информация</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            {{----------------------------- MAIN TAB -----------------------------------------}}
                            <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                                                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content">
                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="main_lang_{{ $key }}" role="tabpanel">
                                            @include('admin.blog.articles.tabs._main',[
                                               'lang' => $key,
                                               'model' => $model
                                            ])

                                            {!! Constructor::output(\App\Models\Translations\BlogArticleTranslation::class,$key) !!}
                                        </div>
                                    @endforeach

                                    @include('admin.blog.articles._form',['model' => $model])
                                </div>
                            </div>

                            {{----------------------------- SEO TAB -----------------------------------------}}
                            <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#seo_lang_{{ $key }}" role="tab">
                                                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content tabcontent-border">
                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="seo_lang_{{ $key }}" role="tabpanel">
                                            @include('admin.blog.articles.tabs._seo',[
                                               'lang' => $key
                                            ])
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">

                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
