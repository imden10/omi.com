@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Заказы</li>
        </ol>
    </nav>

    @include('admin.orders._modal_change_status')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Статус</label>
                            <select name="status" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                @foreach(\App\Models\Orders::getStatuses() as $key => $item)
                                    <option value="{{$key}}" @if(old('status', request()->input('status')) == (string)$key) selected @endif>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Фильтровать</button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('orders.index') }}" class="btn btn-danger form-control text-white">Сбросить</a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Имя</th>
                            <th>Телефон</th>
                            <th>E-mail</th>
                            <th>Продукт</th>
                            <th>Статус</th>
                            <th>Дата заказа</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr data-id="{{$item->id}}">
                                <td>{{$item->id}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->email}}</td>
                                <td>
                                    @if(isset($item->product))
                                        {{$item->product->name}}
                                    @else
                                        <span style="color: #8a1f11">
                                            Нет такого продукта (ID:{{$item->product_id}})
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    {{\App\Models\Orders::getStatuses()[$item->status]}}
                                    <span class="btn btn-primary btn-xs float-right" data-toggle="modal" data-target="#change_status_btn" data-order="{{$item->id}}" data-status="{{$item->status}}">Сменить</span>
                                </td>
                                <td>{{$item->created_at->format('d.m.Y H:i')}}</td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('orders.destroy', $item->id) }}" method="POST">

                                                @csrf
                                                @method('DELETE')

                                                <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('Вы пытаетесь удалить запись?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
