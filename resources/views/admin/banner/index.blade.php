@extends('layouts.admin.app')

@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-nav">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
        <li class="breadcrumb-item active" aria-current="page">Баннер на головній</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('banner.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-3">Зображення</div>
                        <div class="col-md-9">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="image" id="myInput" aria-describedby="myInput">
                                    <label class="custom-file-label" for="myInput">Зображення</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($model[3]->value)
                        <div class="form-group row">
                            <div class="col-md-3">Попередній перегляд</div>
                            <div class="col-md-9">
                                <img src="/uploads/{{$model[3]->value}}" style="max-height: 400px" class="img-thumbnail">
                            </div>
                        </div>
                    @endif

                    <div class="form-group row">
                        <label class="col-md-3 text-right" for="main_banner_title">Заголовок 1</label>
                        <div class="col-md-9">
                            <input type="text" name="main_banner_title" id="main_banner_title" class="form-control" value="{{$model[0]->value}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 text-right" for="main_banner_title">Заголовок 2</label>
                        <div class="col-md-9">
                            <input type="text" name="main_banner_sub_title" id="main_banner_title" class="form-control" value="{{$model[1]->value}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 text-right" for="main_banner_title">Текст кнопки</label>
                        <div class="col-md-9">
                            <input type="text" name="main_banner_btn_text" id="main_banner_title" class="form-control" value="{{$model[2]->value}}">
                        </div>
                    </div>

                    <div class="form-group row float-right">
                        <input type="submit" value="Зберегти" class="btn btn-success text-white">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            /* show file value after file select */
            document.querySelector('.custom-file-input').addEventListener('change',function(e){
                let fileName = document.getElementById("myInput").files[0].name;
                let nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
            });
        });
    </script>
@endpush
