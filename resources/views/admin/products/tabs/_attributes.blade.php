<?php
$attrNames = \App\Models\Translations\AttributeTranslation::query()->where('lang',$lang)->pluck('name','attributes_id')->toArray();

$selected = \App\Models\ProductAttributes::query()->where('lang',$lang)->where('product_id',$model->id)->get();

$hintList = \App\Models\ProductAttributes::query()->where('lang',$lang)->pluck('value')->toArray();
$hintList = array_unique($hintList);

$existAttrIds = [];
?>

@foreach ($model->categories as $key => $category)
    @foreach ($category->attributes as $attr)
        @if(! in_array($attr->id,$existAttrIds))
        <div class="attr-container">
            <?php $total = 0;
                foreach ($selected as $sel){
                    if($sel->attribute_id == $attr->id) $total++;
                }
            ?>
            <?php $flag = false; $col = 0 ?>
                    @foreach($selected as $sel)
                @if($sel->attribute_id == $attr->id)
                    <?php $flag = true; $col++ ?>
                    <div class="form-group row attr-element">
                        <div class="col-md-4">
                            <input type="text" class="form-control" disabled value="{{$attrNames[$attr->id] ?? '-'}}">
                            <input type="hidden" name="attr[{{ $lang }}][id][]" class="form-control" value="{{$attr->id}}">
                        </div>

                        <div class="col-md-4">
                            <input type="text" name="attr[{{ $lang }}][val][]" class="form-control" value="{{$sel->value}}" list="hint-data-list{{$lang}}">
                        </div>

                        <div class="col-md-4 btns-group">
                            @if($col !== 1)
                                <span class="btn btn-danger text-white remove-attr-element">
                                    <i class="mdi mdi-minus"></i>
                                </span>
                            @endif
                            @if($total == $col)
                                <span class="btn btn-primary add-attr-element">
                                    <i class="mdi mdi-plus"></i>
                                </span>
                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
            @if(! $flag)
                    <div class="form-group row attr-element">
                        <div class="col-md-4">
                            <input type="text" class="form-control" disabled value="{{$attrNames[$attr->id] ?? '-'}}">
                            <input type="hidden" name="attr[{{ $lang }}][id][]" class="form-control" value="{{$attr->id}}">
                        </div>

                        <div class="col-md-4">
                            <input type="text" name="attr[{{ $lang }}][val][]" class="form-control" list="hint-data-list{{$lang}}">
                        </div>

                        <div class="col-md-4 btns-group">
                            <span class="btn btn-primary add-attr-element">
                                <i class="mdi mdi-plus"></i>
                            </span>
                        </div>
                    </div>
            @endif
        </div>
        @endif
        <?php $existAttrIds[] = $attr->id; ?>
    @endforeach
@endforeach

<datalist id="hint-data-list{{$lang}}">
    @foreach($hintList as $item)
        <option value="{{$item}}">
    @endforeach
</datalist>
