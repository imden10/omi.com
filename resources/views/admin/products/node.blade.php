<?php $selCats = \App\Models\CategoryProducts::query()->where('products_id', $model->id)->pluck('category_id')->toArray()?>

@foreach($categories as $item)

    @php($category_name[] = $item['title'])

    @if($item['parent_id'] == 0)
        <optgroup label="{{ $item['title'] }}">
    @endif

    <option value="{{ $item['id'] }}" @if(in_array($item['id'],$selCats)) selected @endif>{{ trim(implode(' > ', $category_name), '>') }}</option>

    @if(!empty($item['children']))
        @php($categories = $item['children'])
        @include('admin.products.node')
    @endif

    @if($item['parent_id'] == 0)
        </optgroup>
    @endif

    @php(array_pop($category_name))

@endforeach
