<?php
/* @var \App\Models\ProductPrices $priceModel */
/* @var boolean $opened */

$r = rand(1000,9999);
?>
<div class="card card-price-element">
    <input type="hidden" class="data-changed" value="0"/>
    <div class="card-header" style="padding: 5px 10px 0 10px;">
        @if(isset($priceModel->count_position_unit_id))
            <span style="margin-top: 8px;display: inline-block;font-weight: 600;color: darkslateblue;">{{$priceModel->count_position}} {{App\Models\ProductPrices::getUnits()[$priceModel->count_position_unit_id]}}</span>
        @endif
        <span class="btn btn-outline-primary float-right btn-xs" data-toggle="collapse" href="#collapseExample{{$r}}" style="margin-top: 4px; margin-left: 10px" title="Скрыть">
            <i class="mdi mdi-minus"></i>
        </span>

        <span class="btn btn-danger text-white float-right btn-xs btn-remove-price-elem" data-id="{{$priceModel->id}}" style="margin-top: 4px; margin-left: 10px" title="Удалить">
            <i class="mdi mdi-delete"></i>
        </span>

        <span class="btn btn-success text-white float-right btn-xs btn-save-price-elem" data-id="{{$priceModel->id}}" style="margin-top: 4px;" title="Сохранить">
            <i class="mdi mdi-content-save"></i>
        </span>

        <div class="material-switch float-right" style="margin-right: 10px;vertical-align: middle" title="Статус">
            <input id="someSwitchOptionSuccess_{{$r}}" class="pf-status pf-field" value="1" @if($priceModel->status) checked @endif type="checkbox" />
            <label for="someSwitchOptionSuccess_{{$r}}" class="label-success"></label>
        </div>

        <div class="input-group col-md-3 col-xs-6 col-sm-4 float-right">
            <div class="input-group-prepend">
                <span class="input-group-text">Сортировка</span>
            </div>
            <input type="number" class="form-control pf-order pf-field" value="{{$priceModel->order}}">
        </div>
    </div>
    <div class="card-body collapse @if($opened) show @endif" id="collapseExample{{$r}}">
        <div class="form-group row">
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Количество</span>
                    </div>
                    <input type="text" class="form-control pf-count_position pf-field" value="{{$priceModel->count_position}}">
                    <div class="input-group-append">
                        <select class="form-control pf-count_position_unit_id pf-field">
                            @foreach(\App\Models\ProductPrices::getUnits() as $id => $item)
                                <option value="{{$id}}" @if($id == $priceModel->count_position_unit_id) selected @endif>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Розница до</span>
                    </div>
                    <div class="input-group-prepend">
                        <input type="number" class="form-control pf-retail_count pf-field" value="{{$priceModel->retail_count}}">
                    </div>
                    <div class="input-group-prepend">
                        <select class="form-control pf-retail_count_unit_id pf-field">
                            @foreach(\App\Models\ProductPrices::getUnits() as $id => $item)
                                <option value="{{$id}}" @if($id == $priceModel->retail_count_unit_id) selected @endif>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group-prepend">
                        <input type="text" class="form-control pf-retail_price pf-field" value="{{$priceModel->retail_price}}">
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text">грн.</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Оптом от</span>
                    </div>
                    <div class="input-group-prepend">
                        <input type="number" class="form-control pf-wholesale_count pf-field" value="{{$priceModel->wholesale_count}}">
                    </div>
                    <div class="input-group-prepend">
                        <select class="form-control pf-wholesale_count_unit_id pf-field">
                            @foreach(\App\Models\ProductPrices::getUnits() as $id => $item)
                                <option value="{{$id}}" @if($id == $priceModel->wholesale_count_unit_id) selected @endif>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group-prepend">
                        <input type="text" class="form-control pf-wholesale_price pf-field" value="{{$priceModel->wholesale_price}}">
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text">грн.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
