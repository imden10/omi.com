@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item"><a href="{{route('products.index')}}">Продукты</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>

@if(isset($model->categories[0]->path))
    <div style="display: flex; justify-content: flex-end;margin-bottom: 10px;">
        <a href="{{$model->categories[0]->path}}/{{$model->slug}}" target="_blank" title="Посмотреть на сайте" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Посмотреть на сайте</a>
    </div>
@else
<div class="alert alert-danger">
    У продукта нет категории
</div>
@endif
    <form class="form-horizontal" method="POST" action="{{route('products.update', $model->id)}}">
        @method('PUT')
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                            <li class="nav-item">
                                <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Информация</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="price-tab" data-toggle="tab" href="#price" role="tab" aria-controls="price" aria-selected="false">Цена</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="attributes-tab" data-toggle="tab" href="#attributes" role="tab" aria-controls="attributes" aria-selected="false">Атрибуты</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            {{----------------------------- MAIN TAB -----------------------------------------}}
                            <div class="tab-pane fade show active" id="main">
                                <ul class="nav nav-tabs">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#main_lang_{{ $key }}">
                                                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content">
                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="main_lang_{{ $key }}">
                                            @include('admin.products.tabs._main',[
                                               'lang'  => $key,
                                               'model' => $model,
                                               'data' => $data,
                                            ])

                                            {!! Constructor::output($model->getTranslation($key),$key) !!}
                                        </div>
                                    @endforeach

                                    @include('admin.products._form',['model' => $model])
                                </div>
                            </div>

                            {{----------------------------- Price TAB -----------------------------------------}}
                            <div class="tab-pane fade" id="price">
                                @include('admin.products.tabs._price',[])
                            </div>

                            {{----------------------------- Attributes TAB -----------------------------------------}}
                            <div class="tab-pane fade" id="attributes">
                                <ul class="nav nav-tabs">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#attributes_lang_{{ $key }}">
                                                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content">
                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="attributes_lang_{{ $key }}">
                                            @include('admin.products.tabs._attributes',[
                                               'lang'  => $key,
                                               'model' => $model,
                                            ])
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <input type="submit" value="Сохранить" class="btn btn-success btn-save-all text-white float-right">

                    </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5897fb !important;
            border: 1px solid #5897fb !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color:#5897fb !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e9ecef;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e9ecef;
            color: #3e5569;
            background-color: #fff;
            border-color: rgba(0,0,0,0.25);
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        .card-price-element.not-save .card-header {
            background-color: #ff070773;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $(".select2-field").select2();

            $('#myTab .nav-item a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                let mainThis = $(this);
                let tab = $(this).attr('aria-controls');

                if(tab === 'price') {
                    $('.btn-save-all').hide();
                } else {
                    $('.prices-container .card-price-element').each(function(){
                        let _this = $(this);
                        let changed = _this.find('.data-changed').val();

                        if(changed === '1'){
                            _this.addClass('not-save');

                            e.preventDefault();

                            Swal.fire({
                                title: 'Постойте!',
                                text: "Вы не сохранили данные?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Сохранить!',
                                cancelButtonText: 'Нет'
                            }).then((result) => {
                                if (result.value) {
                                    _this.find('.btn-save-price-elem').trigger('click');
                                    _this.removeClass('not-save');
                                    _this.find('.data-changed').val('0');
                                    mainThis.trigger('show.bs.tab');
                                } else {
                                    _this.removeClass('not-save');
                                    _this.find('.data-changed').val('0');
                                    mainThis.trigger('show.bs.tab');
                                }
                            });
                        }
                    });

                    $('.btn-save-all').show();
                }
            })

            $(document).on('click','.add-attr-element',function () {
                let $container = $(this).closest('.attr-container');

                let $cloneElem = $(this).closest('.attr-element').clone();

                $container.append($cloneElem);

                if($cloneElem.find('.btns-group').find('.remove-attr-element').length == 0){
                    $cloneElem.find('.btns-group').prepend('<span class="btn btn-danger text-white remove-attr-element"><i class="mdi mdi-minus"></i></span>');
                }
                $(this).remove();
            });

            $(document).on('click','.remove-attr-element',function () {
                if($(this).siblings('.add-attr-element').length == 1){
                    $(this).closest('.attr-container .attr-element').last().prev().find('.btns-group').append('<span class="btn btn-primary add-attr-element"><i class="mdi mdi-plus"></i></span>');
                }

                $(this).closest('.attr-element').remove();
            });

            $(".btn-add-price-elem").on('click',function () {
                let product_id = $(this).data('product_id');
                $.ajax({
                    url:"{{route('products.add-price')}}",
                    type:"post",
                    data:{
                        _token:"{{csrf_token()}}",
                        product_id:product_id
                    },
                    success:function(res){
                        $('.prices-container').append(res);
                    }
                });
            });

            $(document).on('click', ".btn-remove-price-elem", function () {
                let id = $(this).data('id');
                let _this = $(this);

                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить запись?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url:"{{route('products.remove-price')}}",
                            type:"post",
                            data:{
                                _token:"{{csrf_token()}}",
                                id:id
                            },
                            success:function(res){
                                _this.closest('.card-price-element').remove();

                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: res.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                        });
                    }
                });
            });

            $(document).on('click', ".btn-save-price-elem", function () {
                let id = $(this).data('id');

                let _this = $(this);

                let status = _this.closest('.card-price-element').find('.pf-status').prop('checked');

                $.ajax({
                    url:"{{route('products.save-price')}}",
                    type:"post",
                    dataType:"json",
                    data:{
                        _token:"{{csrf_token()}}",
                        id:id,
                        count_position:_this.closest('.card-price-element').find('.pf-count_position').val(),
                        count_position_unit_id:_this.closest('.card-price-element').find('.pf-count_position_unit_id').val(),
                        retail_count:_this.closest('.card-price-element').find('.pf-retail_count').val(),
                        retail_count_unit_id:_this.closest('.card-price-element').find('.pf-retail_count_unit_id').val(),
                        retail_price:_this.closest('.card-price-element').find('.pf-retail_price').val(),
                        wholesale_count:_this.closest('.card-price-element').find('.pf-wholesale_count').val(),
                        wholesale_count_unit_id:_this.closest('.card-price-element').find('.pf-wholesale_count_unit_id').val(),
                        wholesale_price:_this.closest('.card-price-element').find('.pf-wholesale_price').val(),
                        order:_this.closest('.card-price-element').find('.pf-order').val(),
                        status:status ? '1' : '0',
                    },
                    success:function(res){
                        if(res.success){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: res.message,
                                showConfirmButton: false,
                                timer: 1500
                            });

                            _this.closest('.card-price-element').replaceWith(res.elem)
                        }
                    }
                });
            });

            $(document).on('change', ".pf-field", function () {
                $(this).closest('.card-price-element').find('.data-changed').val('1');
            })
        });
    </script>
@endpush
