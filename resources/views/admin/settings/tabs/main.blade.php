<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_test_{{ $key }}">Тест</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][test]" value="{{ old('setting_data.' . $key . '.test', $data[$key]['test'][0]['value'] ?? '') }}" id="setting_test_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.test') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_contact_center_{{ $key }}">Контакт-центр</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][contact_center]" value="{{ old('setting_data.' . $key . '.contact_center', $data[$key]['contact_center'][0]['value'] ?? '') }}" id="setting_contact_center_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.contact_center') ? ' is-invalid' : '' }}">
                </div>
            </div>
        </div>
    @endforeach
</div>








