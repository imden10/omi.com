@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item" aria-current="page">Настройки</li>
            <li class="breadcrumb-item active" aria-current="page">{{\App\Models\Settings::getTabNames()[$tab]}}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <i class="mdi mdi-settings"></i>
                    <span style="text-transform: uppercase">{{\App\Models\Settings::getTabNames()[$tab]}}</span>
                </div>
                <div class="card-body">
                    <form action="{{route('settings.save')}}" method="post" class="form-horizontal">
                        @csrf
                        @include('admin.settings.tabs.' . $tab, ['data' => $data])

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
