@foreach($categories as $item)

    @php($category_name[] = $item['title'])

    @if($item['parent_id'] == 0)
        <optgroup label="{{ $item['title'] }}">
    @endif

    <option value="{{ $item['id'] }}">{{ trim(implode(' > ', $category_name), '>') }}</option>

    @if(!empty($item['children']))
        @php($categories = $item['children'])
        @include('admin.menu.category_node')
    @endif

    @if($item['parent_id'] == 0)
        </optgroup>
    @endif

    @php(array_pop($category_name))

@endforeach
