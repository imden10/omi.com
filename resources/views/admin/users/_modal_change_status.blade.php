<div id="change_status_btn" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form action="{{route('user.change-status')}}" method="POST">
            <input type="hidden" name="user_id" id="user_id">
        @csrf
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <select name="status" class="form-control" id="select_status">
                    @foreach(\App\Models\User::getStatuses() as $key => $status)
                        <option value="{{$key}}">{{$status}}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
                <input type="submit" class="btn btn-success" value="Змінити">
            </div>
        </div>
        </form>
    </div>
</div>

<script>
    $('#change_status_btn').on('show.bs.modal', function(e) {
        let user = e.relatedTarget.dataset.user;
        let status = e.relatedTarget.dataset.status;
        $("#user_id").val(user);
        $("#select_status").val(status);
    });
</script>
