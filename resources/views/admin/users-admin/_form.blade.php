@if($action === 'create')
    <form class="form-horizontal" method="POST" action="{{ route('users-admin.store') }}">
@elseif($action === 'edit')
    <form action="{{route('users-admin.update', $model->id)}}" class="form-horizontal" method="post">
        @method('PUT')
@endif
    @csrf

        <div class="form-group row">
            <label class="col-md-3 text-right" for="name"><span style="color:red">*</span>Ім'я</label>
            <div class="col-md-9">
                <input type="text" name="name" id="name" class="form-control" value="{{$model->name}}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="email"><span style="color:red">*</span> E-mail</label>
            <div class="col-md-9">
                <input type="text" name="email" id="email" class="form-control" value="{{$model->email}}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="phone">Телефон</label>
            <div class="col-md-9">
                <input type="text" name="phone" id="phone" class="form-control" value="{{$model->phone}}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="password">
                @if(! $model->id)
                <span style="color:red">*</span>
                @endif
                Пароль</label>
            <div class="col-md-9">
                <input type="password" name="password" id="password" class="form-control">
            </div>
        </div>

        <input type="submit" class="btn btn-success text-white" value="Зберегти">
</form>

@push('scripts')
    <script>
        $(document).ready(() => {

        });
    </script>
@endpush
