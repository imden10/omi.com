@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Категории</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('category.create')}}" class="btn btn-primary float-right">
                    <i class="fa fa-plus"></i>
                    Добавить
                </a>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Статус</label>
                            <select name="status" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                @foreach(\App\Models\Category::getStatuses() as $key => $item)
                                    <option value="{{$key}}" @if(old('status', request()->input('status')) == (string)$key) selected @endif>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Поиск</label>
                            <input type="text" class="form-control" name="name" value="{{old('name', request()->input('name'))}}">
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Фильтровать</button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('category.index') }}" class="btn btn-danger form-control text-white">Сбросить</a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>URL</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr data-id="{{$item->id}}">
                                <td>{{$item->id}}</td>
                                <td>
                                    <a href="{{ route('category.edit', $item->id) }}">
                                        {{$item->getNameWithPath()}}
                                    </a>
                                </td>
                                <td>{{$item->slug}}</td>
                                <td>{{\App\Models\Category::getStatuses()[$item->status]}}</td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('category.destroy', $item->id) }}" method="POST">

                                                <a href="/catalog/{{$item->path}}" target="_blank" class="btn btn-info btn-xs" title="Посмотреть на сайте"><i class="fa fa-eye"></i></a>

                                                <a href="{{ route('category.edit', $item->id) }}" class="btn btn-xs btn-primary">
                                                    <i class="fas fa-edit fa-lg"></i>
                                                </a>

                                                @csrf
                                                @method('DELETE')

                                                <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('Вы пытаетесь удалить запись?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
