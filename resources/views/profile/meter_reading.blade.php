@extends('layouts.app')

@section('content')
    @include('profile._header')
    @include('profile._menu')

    <div>
        Надати показники лічильника

        <form method="post">
            @csrf
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <input type="text" name="value">
            <input type="submit" value="Відправити">
        </form>
    </div>
@endsection

@push('scripts')

@endpush
