<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{asset('css/auth/cabinetStyle.css')}}">
</head>
<body>
<div id="request_app" class="main">
    <request_form :user_id="{{\Illuminate\Support\Facades\Auth::user()->id}}"
                  :request_id="{{$request ? $request->id : 0}}"
                  :request_status="{{$request ? $request->status : 0}}"
                  :revisions="{{$revisions}}"
                  :user_premises_owner="{{$user_premises_owner}}"
                  :operator_grm_list="{{json_encode(\App\Models\Requests::getOperatorGrmList())}}"
    ></request_form>
</div>

<footer>
    <div class="payment">
        <img src="/img/cab/pitem1.svg" alt="">
        <img src="/img/cab/pitem2.svg" alt="">
        <img src="/img/cab/pitem3.svg" alt="">
    </div>
    <a href="#" class="copyright">Created by <b>OwlWeb Team</b></a>
</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="{{asset('js/request.js')}}"></script>
</body>
</html>

