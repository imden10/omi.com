@extends('layouts.auth.app')

@section('content')
    <div class="main">
        <div class="leftSide">
            <a href="#" class="logo">
                <img src="/img/cab/logo.svg" alt="">
            </a>
            <div class="priceTxt">Ціна на вересень</div>
            <div class="priceVal">6.73<span> ₴/м3</span></div>
        </div>
        <div class="centerSide">
            <div class="anketWrp">
                <div class="anketa">
                    <div class="infoDiv">
                        <div class="deco">☝</div>
                        <p>Шановний клієнт, якщо ви не є власником об’єкта, який хочете приєднати до Нашого Газу, то вам необхідно мати довіреність від власника приміщення або нотаріальний договір оренди (користування) приміщення. В довіреності або договорі оренди має обов’язково бути пункт про дозвіл на право підпису таких документів користувачем приміщення.</p>
                        <p>Заповнюючи заяву вам необхідно вказувати особисті дані ВЛАСНИКА приміщення, після того як документ заява-приєднання буде сформований його має підписати довірена особа.</p>
                        <p>Далі вам необхідно обов’язково додати довіреність або договір оренди до заяви. Та ? паспорт і код. Після підключення оплату за газопостачання буде здійснювати довірена особа.</p>
                    </div>
                    <div class="botActions">
                        <a href="{{route('profile.owner-check')}}" class="controlBtn back">
                            <svg width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.55078 11.0011L11.4503 1.10156M1.55078 11.0011L11.4503 20.9006M1.55078 11.0011H20.4503" stroke-width="2"></path>
                            </svg>
                        </a>
                        <a href="javascript:return void(0)" class="controlBtn" id="i_not_owner_link">Продовжити</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="rightSide">
            <div></div>
            <div>
                <div class="contactWrp">
                    <a href="#">
                        <img src="/img/cab/proneic.svg" alt=""> 800 000 00 00
                    </a>
                    <a href="#">
                        <img src="/img/cab/logos_telegram.svg" alt=""> Telegram
                    </a>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('profile.owner-check') }}" class="form" method="POST">
    @csrf
        <input type="hidden" name="premises_owner" value="{{\App\Models\User::PREMISES_OWNER_NO}}">
        <input type="submit" id="form_owner_btn" style="display: none">
    </form>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $("#i_not_owner_link").on('click', function () {
                $("#form_owner_btn").trigger('click');
            });
        });
    </script>
@endpush
