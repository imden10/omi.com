@extends('layouts.auth.app')

@section('content')
    <div class="main">
        <div class="leftSide">
            <a href="#" class="logo">
                <img src="/img/cab/logo.svg" alt="">
            </a>
            <div class="priceTxt">Ціна на вересень</div>
            <div class="priceVal">6.73<span> ₴/м3</span></div>
        </div>
        <div class="centerSide">
            <div class="anketWrp">
                <div class="anketa">
                    <h1>Оберіть ким ви є</h1>
                    <a href="javascript:return void(0)" id="i_owner_link" class="wideActionLink2">
                        <span class="txt">Я - власник приміщення</span>
                        <div class="deco"><img src="/img/cab/rightArr.svg" alt=""></div>
                    </a>
                    <a href="{{route('profile.not-owner-instruction')}}" class="wideActionLink2">
                        <span class="txt">Я - не власник</span>
                        <div class="deco"><img src="/img/cab/rightArr.svg" alt=""></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="rightSide">
            <div></div>
            <div>
                <div class="contactWrp">
                    <a href="#">
                        <img src="/img/cab/proneic.svg" alt=""> 800 000 00 00
                    </a>
                    <a href="#">
                        <img src="/img/cab/logos_telegram.svg" alt=""> Telegram
                    </a>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('profile.owner-check') }}" class="form" method="POST">
    @csrf
        <input type="hidden" name="premises_owner" value="{{\App\Models\User::PREMISES_OWNER_YES}}">
        <input type="submit" id="form_owner_btn" style="display: none">
    </form>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $("#i_owner_link").on('click', function () {
                $("#form_owner_btn").trigger('click');
            });
        });
    </script>
@endpush
