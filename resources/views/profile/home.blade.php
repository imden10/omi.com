@extends('layouts.app')

@section('content')
    @if($request_status)
        @if($request_status === \App\Models\Requests::STATUS_ON_CHECK)
            <h4>Заявка проходить перевірку</h4>
        @elseif($request_status === \App\Models\Requests::STATUS_SUCCESS)
            <h4>Очікує підключення</h4>
        @endif
    @elseif($user->hasRequest())
        @if($user->status == \App\Models\User::STATUS_CONNECTION)
            @include('profile.profile',['user' => $user])
        @elseif($user->status == \App\Models\User::STATUS_WAITING_CONNECTION)
            <h4>Очікує підключення</h4>
        @endif
    @else
        <a href="{{route('profile.request')}}">Оформлення заяви</a>
    @endif
@endsection

@push('scripts')

@endpush
