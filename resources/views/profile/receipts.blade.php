@extends('layouts.app')

@section('content')
    @include('profile._header')
    @include('profile._menu')

    <div style="display: flex">
    @foreach($user->receiptNotPaid as $receipt)
        <div>
            <a href="{{asset('uploads/user-receipts')}}/{{$receipt->file}}" target="_blank" style="margin-right: 15px">
                <img src="{{asset('uploads/user-receipts')}}/{{$receipt->file}}" style="width: 200px; height: auto">
            </a><br>
            <form method="post" class="receipt-form">
                @csrf
                <input type="hidden" name="id" value="{{$receipt->id}}">
                <input type="submit" value="Підтвердити оплату">
            </form>
        </div>
    @endforeach
    </div>

@endsection

@push('scripts')
    <script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $(".receipt-form input[type='submit']").on('click', function (e) {
                e.preventDefault();

                if(confirm('Ви вже оплатили квитанцію, якщо так натисніть "Ok"')){
                    $(this).closest('.receipt-form').submit();
                }
            })
        });
    </script>
@endpush
