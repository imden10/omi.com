header

@if(count($user->receiptNotPaid))
    <div>
        <span>Квитанції для оплати:</span>
        <span>{{count($user->receiptNotPaid)}}</span>
        <a href="/profile/receipts">Переглянути</a>
    </div>
@else
    <span>Квитанцій для оплати немає</span>
@endif
