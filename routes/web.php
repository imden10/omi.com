<?php

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Support\Facades\Route;

/* @var $localeMiddleware LocaleMiddleware */
$localeMiddleware = app( LocaleMiddleware::class);

\Illuminate\Support\Facades\Auth::routes();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('cache-clear',function (){
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');

    return redirect()->back()->with('success','Кеш успешно очищено!');
})->name('cache-clear');

//lang. switcher
//Переключение языков
Route::get('setlocale/{lang}', function ($lang) use($localeMiddleware) {

    $referer = \Illuminate\Support\Facades\Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); // URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1],$localeMiddleware->languages)) {

        unset($segments[1]); //удаляем метку
    }

    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if ($lang != $localeMiddleware->mainLanguage) {
        array_splice($segments, 1, 0, $lang);
    }

    //формируем полный URL
    $url = request()->root() . implode("/", $segments);

    //если были еще GET-параметры - добавляем их
    if (parse_url($referer, PHP_URL_QUERY)) {
        $url = $url . '?' . parse_url($referer, PHP_URL_QUERY);
    }

    return redirect(rtrim($url, '/\\')); //Перенаправляем назад на ту же страницу

})->name('setlocale');

Route::any('/admin/login', [\App\Http\Controllers\Admin\HomeController::class,'login'])->name('admin.login');

/*********************************************** ADMIN ****************************************************************/
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::post('/export-table', [\App\Http\Controllers\Admin\ExportTableController::class,'export'])->name('export-table');
    Route::get('/', [\App\Http\Controllers\Admin\HomeController::class,'index'])->name('admin');
    Route::resource('/users-admin', \App\Http\Controllers\Admin\UserAdminController::class);
    Route::get('/users/{id}/{tab}', [\App\Http\Controllers\Admin\UsersController::class,'getTab'])->where('tab', '[a-z0-9-]+');
    Route::post('/users-change-status', [\App\Http\Controllers\Admin\UsersController::class,'changeStatus'])->name('user.change-status');
    Route::resource('/users', \App\Http\Controllers\Admin\UsersController::class);
    Route::resource('/pages', \App\Http\Controllers\Admin\PagesController::class);
    Route::post('/orders/status/change', [\App\Http\Controllers\Admin\OrdersController::class,'changeStatus'])->name('orders.status-change');
    Route::resource('/orders', \App\Http\Controllers\Admin\OrdersController::class);
    Route::resource('/services', \App\Http\Controllers\Admin\ServicesController::class);
    Route::post('menu/move', [\App\Http\Controllers\Admin\MenuController::class,'move']);
    Route::post('menu/rebuild', [\App\Http\Controllers\Admin\MenuController::class,'rebuild']);
    Route::post('menu/add-item', [\App\Http\Controllers\Admin\MenuController::class,'addItem'])->name('menu.add-item');
    Route::post('menu/delete-menu', [\App\Http\Controllers\Admin\MenuController::class,'deleteMenu'])->name('menu.delete-menu');
    Route::post('menu/add-menu', [\App\Http\Controllers\Admin\MenuController::class,'addMenu'])->name('menu.add-menu');
    Route::resource('/menu', \App\Http\Controllers\Admin\MenuController::class);
    Route::resource('/faq', \App\Http\Controllers\Admin\FaqController::class);
    Route::post('/attributes/delete-selected', [\App\Http\Controllers\Admin\AttributesController::class,'deleteSelected'])->name('attributes.delete-selected');
    Route::post('/attributes.sort', [\App\Http\Controllers\Admin\AttributesController::class,'sort'])->name('attributes.sort');
    Route::resource('/attributes', \App\Http\Controllers\Admin\AttributesController::class);
    Route::resource('/category', \App\Http\Controllers\Admin\CategoriesController::class);
    Route::get('/products/generate-recommendations', [\App\Http\Controllers\Admin\ProductsController::class,'generateRecommendationProducts']);
    Route::post('/products/price/add', [\App\Http\Controllers\Admin\ProductsController::class,'addPrice'])->name('products.add-price');
    Route::post('/products/price/save', [\App\Http\Controllers\Admin\ProductsController::class,'savePrice'])->name('products.save-price');
    Route::post('/products/price/remove', [\App\Http\Controllers\Admin\ProductsController::class,'removePrice'])->name('products.remove-price');
    Route::resource('/products', \App\Http\Controllers\Admin\ProductsController::class);
    Route::get('settings/{tab}', [\App\Http\Controllers\Admin\SettingsController::class,'index'])->name('add-notice')->where('tab', '[a-z0-9-]+');
    Route::post('settings/save', [\App\Http\Controllers\Admin\SettingsController::class,'save'])->name('settings.save');
    Route::post('widget/get-products-by-category', [\App\Http\Controllers\Admin\HomeController::class,'widgetGetProductsByCategory'])->name('widget.get-products-by-category');

    /** BLOG **/
    Route::group(['prefix' => 'blog'], function () {
        Route::resource('/tags', \App\Http\Controllers\Admin\BlogTagsController::class);
        Route::post('categories/rebuild', [\App\Http\Controllers\Admin\BlogCategoriesController::class,'rebuild']);
        Route::resource('/categories', \App\Http\Controllers\Admin\BlogCategoriesController::class);
        Route::resource('/articles', \App\Http\Controllers\Admin\BlogArticlesController::class);
        Route::get('/subscribe', [\App\Http\Controllers\Admin\BlogSubscribeController::class,'index'])->name('subscribe.index');
        Route::get('/subscribe/list', [\App\Http\Controllers\Admin\BlogSubscribeController::class,'list'])->name('subscribe.list');
    });
});
/************************************ END ADMIN ***********************************************************************/


/****************************************** PROFILE *******************************************************************/
Route::group(['prefix' => 'profile', 'middleware' => ['auth', 'profile']], function () {
    Route::get('/', [\App\Http\Controllers\Profile\HomeController::class,'index'])->name('profile');
});
/***************************************** END PROFILE ****************************************************************/


/***************************** FRONT *******************************/
Route::group(['prefix' => $localeMiddleware->getLocale()], function () {
    Route::post('add-notice', [\App\Http\Controllers\Front\TelegramNoticeController::class,'addNotice'])->name('add-notice');
    Route::post('/subscribe', [\App\Http\Controllers\Front\BlogSubscribeController::class,'index'])->name('front.subscribe');
    Route::get('/blog', [\App\Http\Controllers\Front\BlogController::class,'index'])->name('blog');
    Route::get('/blog/{slug}', [\App\Http\Controllers\Front\BlogController::class,'single'])->where('slug', '[a-z0-9-]+');
    Route::get('/blog/tag/{slug}', [\App\Http\Controllers\Front\BlogController::class,'tags'])->where('slug', '[a-z0-9-]+');

    Route::get('/service/{slug}', [\App\Http\Controllers\Front\ServiceController::class,'show'])->where('slug', '[a-z0-9-]+')->name('service.show');
    Route::get('/catalog/{slug}/{stringfilter?}', [\App\Http\Controllers\Front\CategoryController::class,'show'])->where('stringfilter', '.*')->name('category.show');
    Route::post('order-request', [\App\Http\Controllers\Front\ProductController::class,'addRequest'])->name('product.add-request');
    Route::get('{path}/{slug}', [\App\Http\Controllers\Front\ProductController::class,'show'])->where('path', '.*')->name('product.show');

    Route::get('/{slug?}', [\App\Http\Controllers\Front\PageController::class,'index'])->where('slug', '[a-z0-9-]+');
    Route::get('/', [\App\Http\Controllers\Front\PageController::class,'index'])->name('main');
});



//Route::group(['namespace' => 'Front'], function(){
//
//});
/*******************************************************************/



/* API */
Route::group(['namespace' => 'Api', 'prefix' => 'api'], function(){

});
/***************/
