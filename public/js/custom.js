String.prototype.replace_all = function(search, replace) {
    return this.split(search).join(replace);
};

function create_item(template, container, placeholder) {
    const cloneItem = template
        .clone()[0]
        .outerHTML
        .replace_all(placeholder, get_item_id(container.children()));

    container.append(cloneItem);
}

function get_item_id(items) {
    if (items.length > 0) {
        const array_items = [];

        for (let i = 0; i < items.length; i++) {
            let property_value = +jQuery(items[i]).attr('data-item-id');
            array_items.push(property_value);
        }

        return Math.max.apply(null, array_items) + 1;
    }

    return 1;
}
