<?php

namespace App\Modules\Constructor\Collections;

class ComponentCollections
{
    /**
     * @return array
     */
    public static function widget()
    {
        return [
            'label'  => 'Виджет',
            'params' => [
                'labels'  => [

                ],
                'widgets' => function ($lang) {
                    $widgets = \App\Modules\Widgets\Models\Widget::where('lang', $lang)->get();

                    return ['' => '---'] + collect($widgets)
                            ->mapWithKeys(function ($widget) {
                                return [$widget->id => $widget->name];
                            })
                            ->toArray();
                },
            ],
            'rules'  => [
                'content.widget' => 'nullable|integer',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function imageAndText()
    {
        return [
            'label'  => 'Изображение и текст',
            'params' => [
                'image_positions' => [
                    'right' => 'Праворуч',
                    'left'  => 'Ліворуч',
                ],
                'labels'          => [
                    'title'          => 'Заголовок',
                    'image_position' => 'Позиція зображення',
                ],
            ],
            'rules'  => [
                'content.title'          => 'nullable|string|max:255',
                'content.image'          => 'nullable|string|max:255',
                'content.image_position' => 'required|string|max:255',
                'content.description'    => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function simpleText()
    {
        return [
            'label'  => 'Текст',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                ],
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.description' => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function list()
    {
        return [
            'label'  => 'Список',
            'params' => [
                'type'   => [
                    'ul'       => 'Обычный',
                    'ol'       => 'Нумерованый',
                    'ol_style' => 'Нумерованый стилизованный',
                ],
                'labels' => [
                    'type'  => 'Тип',
                    'item'  => 'Текст',
                    'title' => 'Заголовок',
                ],
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.type'        => 'required|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function statistics()
    {
        return [
            'label'  => 'Статистика',
            'params' => [
                'labels' => [
                    'number' => 'Число',
                    'unit'   => 'Мера',
                    'text'   => 'Текст',
                ],
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function buttonOrderService()
    {
        return [
            'label'  => 'Кнопка `Заказать услугу`',
            'params' => [
                'labels' => [
                    'title' => 'Текст кнопки',
                ],
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function button()
    {
        return [
            'label'  => 'Кнопка',
            'params' => [
                'labels' => [
                    'title' => 'Текст кнопки',
                    'link'  => 'Ссылка',
                ],
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.link'  => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function fullImageAndText()
    {
        return [
            'label'  => 'Изображение на всю ширину',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                ],
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.image' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function text2Columns()
    {
        return [
            'label'  => 'Текст в 2 колонки',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                    'text1' => 'Текст 1',
                    'text2' => 'Текст 2',
                ],
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.text1' => 'nullable|string|max:65000',
                'content.text2' => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function quotes()
    {
        return [
            'label'  => 'Цитата',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                    'text'  => 'Текст',
                ],
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.text'  => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function advantages()
    {
        return [
            'label'  => 'Преимущества',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                    'text'  => 'Текст',
                ],
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function gallery()
    {
        return [
            'label'  => 'Галерея',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                    'image' => 'Изображение',
                ],
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function products()
    {
        return [
            'label'  => 'Рекомендуемые продукты',
            'params' => [
                'labels' => [
                    'title'      => 'Заголовок',
                    'subtitle'   => 'Подзаголовок',
                    'product_id' => 'Продукт'
                ],
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.subtitle'    => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

}

