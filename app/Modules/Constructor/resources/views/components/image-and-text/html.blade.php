@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-12 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.title') }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="d-flex col-12">
            <div>
                <div class="form-group input-group-sm">
                    <select class="form-control @error(constructor_field_name_dot($key, 'content.image_position')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.image_position') }}">
                        <option value="">{{ $params['labels']['image_position'] }}</option>
                        @foreach($params['image_positions'] as $slug => $position)
                            <option value="{{ $slug }}" @if (old(constructor_field_name_dot($key, 'content.image_position'), $content['image_position'] ?? '') == $slug) selected @endif>{{ $position }}</option>
                        @endforeach
                    </select>

                    @error(constructor_field_name_dot($key, 'content.image_position'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                {{ media_preview_box(constructor_field_name($key, 'content.image'), $content['image'] ?? null, $errors) }}
            </div>

            <div class="w-100 ml-3">
                <textarea class="form-control summernote @error(constructor_field_name_dot($key, 'content.description')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.description') }}">{{ old(constructor_field_name_dot($key, 'content.description'), $content['description'] ?? '') }}</textarea>

                @error(constructor_field_name_dot($key, 'content.description'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>
</div>

@include('constructor::layouts.footer')
