<script type="text/javascript">
    (function () {
        $(document).on('click', '.add-gallery-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.gallery-list-template');
            const container = $(this).parent().find('.gallery-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    })();
</script>
