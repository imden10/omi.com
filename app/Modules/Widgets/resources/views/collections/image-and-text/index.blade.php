<section class="infoSection">
    <div class="container">
        <div class="twocolDiv">
            @if($data['image_position'] === 'right')
                <div class="txtdiv">
                    @if(! empty($data['title']))
                        <h3>{{$data['title']}}</h3>
                    @endif
                    @if(! empty($data['text']))
                    {!! $data['text'] !!}
                    @endif
                </div>
            @endif
            <div class="imgdiv">
                <img src="{{get_image_uri($data['image'],'original')}}" alt="">
            </div>
            @if($data['image_position'] === 'left')
                <div class="txtdiv">
                    @if(! empty($data['title']))
                        <h3>{{$data['title']}}</h3>
                    @endif
                    @if(! empty($data['text']))
                        {!! $data['text'] !!}
                    @endif
                </div>
            @endif
        </div>
    </div>
</section>
