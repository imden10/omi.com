<section class="infoSection">
    <div class="container">
        <div class="advantages">
            @if(! empty($data['title']))
                <div class="caption">{{$data['title']}}</div>
            @endif
            @if(isset($data['list']) && count($data['list']))
                <div class="itemsWrp {{$data['columns']}}">
                    @foreach($data['list'] as $item)
                        <div class="item">
                            <img src="{{get_image_uri($item['image'],'original')}}" alt="">
                            <p>{{$item['title']}}</p>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</section>
