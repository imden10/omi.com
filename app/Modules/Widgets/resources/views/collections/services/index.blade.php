<?php
$services = \App\Models\Services::query()->active()->get();
$currentLang = app()->getLocale();
?>
<section class="servicesSecion">
    <div class="container">
        @if(! empty($data['title']))
            <h2>{{$data['title']}}</h2>
        @endif
        @if(! empty($data['title']))
            <p class="subtext">{{$data['subtitle']}}</p>
        @endif
        @if(isset($data['list']) && count($data['list']))
            <div class="serviseLnksWrp">
                @foreach($data['list'] as $item)
                    @foreach($services as $service)
                        @if($item['service_id'] == $service->id)
                            <?php
                                $url = '/service/' . $service->slug;

                                if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                                    $url = '/' . $currentLang . $url;
                                }
                            ?>
                            <div class="itemWrp">
                                <a href="{{$url}}">
                                    <img src="{{get_image_uri($service->image,'original')}}" alt="">
                                    <span>{{$service->name}}</span>
                                </a>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
        @endif
    </div>
</section>
