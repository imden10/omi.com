<section class="consultFormSection" style="background-image: url(/img/recomendblack.jpg);">
    <div class="container">
        <div>
            @if(! empty($data['title']))
                <h2>{{$data['title']}}</h2>
            @endif
                @if(! empty($data['text']))
                    <div class="subtext">{{$data['text']}}</div>
                @endif
        </div>
        <div>
            <form action="#" class="contactForm" id="contactForm" novalidate="novalidate">
                @csrf
                <div class="inputWrp">
                    <input type="text" name="name" placeholder="{{__('name')}}">
                </div>
                <div class="inputWrp">
                    <input type="text" name="email" placeholder="{{__('email')}}">
                </div>
                <div class="inputWrp">
                    <input type="text" name="phone" placeholder="{{__('phone number')}}">
                </div>

                <input type="hidden" name="no_recaptcha" value="1">

                <div class="g-recaptcha"
                     data-sitekey="{{env('GOOGLE_RECAPTCHA_SITE_KEY')}}"
                     data-size="invisible">
                </div>

                <button type="submit">{{$data['btn_name']}}</button>
            </form>
        </div>
        <div>
            <img src="/img/logo.svg" alt="">
        </div>
    </div>
</section>
