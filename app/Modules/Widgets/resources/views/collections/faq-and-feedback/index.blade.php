<?php
    $ids = [];

    if(! empty($data['list']) && count($data['list'])) {
        foreach ($data['list'] as $item) {
            $ids[] = $item['faq_id'];
        }
    }

    $faqs = \App\Models\Faq::query()->whereIn('id',$ids)->get();
?>

<section class="faqSection">
    <div class="container">
        @if(! empty($data['title']))
            <h2>{{$data['title']}}</h2>
        @endif
        <div class="twocolDiv">
            <div>
                @if(count($faqs))
                <div class="faqItemsWrap">
                    @foreach($faqs as $item)
                        <div class="item">
                            <div class="head">
                                <p>{{$item->getTranslation(app()->getLocale())->question}}</p>
                                <img src="/img/chewron.svg" alt="">
                            </div>
                            <div class="itemBody" style="display: none;">
                                <p>{{strip_tags($item->getTranslation(app()->getLocale())->answer)}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
            </div>
            <div>
                <form action="#" id="contactForm" novalidate="novalidate" class="col6Contact" style="background-image: url(/img/recomendblack.jpg);">
                    @csrf
                    @if(! empty($data['form_title']))
                        <div class="caption">{{$data['form_title']}}</div>
                    @endif
                    @if(! empty($data['form_sub_title']))
                        <div class="formSub">{{$data['form_sub_title']}}</div>
                    @endif
                    <div class="inputs">
                        <div class="inputWrp">
                            <input type="text" name="name" placeholder="{{__('name')}}">
                        </div>
                        <div class="inputWrp">
                            <input type="text" name="email" placeholder="{{__('email')}}">
                        </div>
                        <div class="inputWrp">
                            <input type="text" name="phone" placeholder="{{__('phone number')}}">
                        </div>
                        <div class="g-recaptcha"
                             data-sitekey="{{env('GOOGLE_RECAPTCHA_SITE_KEY')}}"
                             data-size="invisible">
                        </div>

                        <button type="submit">{{$data['form_btn_text']}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@push('scripts')
    <script>
        $(document).ready(function () {
            // FAQ items actions
            $('.faqItemsWrap .item .head').on("click", function(){
                $(this).closest('.item').find('.itemBody').slideToggle();
                $(this).closest('.item').toggleClass('active')
            })

        });
    </script>
@endpush
