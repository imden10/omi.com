<section class="mainBanner" style="background-image: url({{get_image_uri($data['image'], 'original')}});">
    <div class="container">
        <div class="leftSide">
            <h1>{{$data['title']}}</h1>
            <p class="sub">{{$data['subtitle']}}</p>
            <p class="sub2">{{$data['subtitle2']}}</p>
        </div>
        <div class="rightSide">
            <div class="subItemsWrp">
                <div class="subItem">
                    <div class="caption">{{$data['column_title1']}}</div>
                    <div class="txt">{{$data['column_subtitle1']}}</div>
                </div>
                <div class="subItem">
                    <div class="caption">{{$data['column_title2']}}</div>
                    <div class="txt">{{$data['column_subtitle2']}}</div>
                </div>
            </div>
            <form action="#" class="bannerContact" id="bannerContact" novalidate="novalidate">
                @csrf
                <div class="inputWrp">
                    <input type="text" name="phone" placeholder="{{__('phone number')}}">
                    <label for="" class="error"></label>
                </div>

                <div class="g-recaptcha"
                     data-sitekey="{{env('GOOGLE_RECAPTCHA_SITE_KEY')}}"
                     data-size="invisible">
                </div>

                <button type="submit">{{$data['btn_name']}}</button>
            </form>
            <div class="undertext">
                <span>{{$data['after_btn_text']}}</span>
            </div>
        </div>
    </div>
</section>
