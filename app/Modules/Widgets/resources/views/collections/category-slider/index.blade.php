<section class="showCaseSlider" style="background-image: url(/img/recomendblack.jpg);">
    <div class="container">
        <h2>{{$data['title']}}</h2>
        <div class="recomendWrap">
            <div class="recomendItemSlides">
                @if(! empty($data['list']))
                    @foreach($data['list'] as $list)
                        <?php
                            $category = \App\Models\Category::query()->where('id',$list['category_id'])->first();
                            $product = \App\Models\Products::query()->where('id',$list['product_id'])->first();
                            $options = isset($category->options) ? json_decode($category->options,true) : [];
                        ?>
                        @if(is_null($category) || is_null($product))
                            @continue;
                        @endif
                        <div class="slide">
                            <div class="slideLeft">
                                <img src="{{get_image_uri($product->image, 'original')}}" alt="">
                                @if(count($product->prices))
	                                @foreach($product->prices as $price)
	                                    @if($loop->last)
	                                        <div class="bottext data-min">{{__("minimum order from")}} {{$price->wholesale_count}} 
	                                        	{{$price->wholesale_count_unit_id ? \App\Models\ProductPrices::getUnits()[$price->wholesale_count_unit_id] : ''}}</div>
	                                    @endif
	                                @endforeach
                                @endif
                            </div>
                            <div class="slideRIght">
                                <div class="caption">{{$product->name}}</div>
                                <div class="advantagesWrap">
                                    @if(count($options))
                                        @foreach($options as $option)
                                            <div class="item">{{$option}}</div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="group">
                                    <div class="packageSelect">
                                        <div class="price">
                                            @foreach($product->prices as $price)
                                                @if($loop->last)
                                                    <span>{{$price->wholesale_price}}</span>{{__('uah')}}
                                                @endif
                                            @endforeach
                                        </div>
                                        <div class="packages">
                                            @foreach($product->prices as $price)
                                                <button data-price="{{$price->wholesale_price}}" data-min="{{__("minimum order from")}} {{$price->wholesale_count}} {{$price->wholesale_count_unit_id ? \App\Models\ProductPrices::getUnits()[$price->wholesale_count_unit_id] : ''}}" 
                                                	@if($loop->last) class="active" @endif
                                                >
                                                	{{$price->count_position}}
                                                	{{$price->count_position_unit_id ? \App\Models\ProductPrices::getUnits()[$price->count_position_unit_id] : ''}}</button>
                                            @endforeach
                                        </div>
                                    </div>
                                    @if(isset($product->categories[0]->path))
                                        <a href="{{$product->categories[0]->path}}/{{$product->slug}}" class="details">{{__('More')}}</a>
                                    @endif
                                </div>
                            </div>
                            <div class="group">
                                <div class="packageSelect">
                                    <div class="price">
                                        @foreach($product->prices as $price)
                                            @if($loop->last)
                                                <span>{{$price->wholesale_price}}</span>{{__('uah')}}
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="packages">
                                        @foreach($product->prices as $price)
                                            <button data-price="{{$price->wholesale_price}}" data-min="{{__("minimum order from")}} {{$price->wholesale_count}} {{$price->wholesale_count_unit_id ? \App\Models\ProductPrices::getUnits()[$price->wholesale_count_unit_id] : ''}}" 
                                            	@if($loop->last) class="active" @endif
                                            	>{{$price->count_position}}
                                            	{{$price->count_position_unit_id ? \App\Models\ProductPrices::getUnits()[$price->count_position_unit_id] : ''}}</button>
                                        @endforeach
                                    </div>
                                </div>
                                @if(isset($product->categories[0]->path))
                                    <a href="{{$product->categories[0]->path}}/{{$product->slug}}" class="details">{{__('More')}}</a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="controls">
                <div class="slideItems">
                    @if(! empty($data['list']))
                        @foreach($data['list'] as $key => $list)
                            <?php
                                $category = \App\Models\Category::query()->where('id',$list['category_id'])->first();
                            ?>
                            @if(is_null($category) || is_null($product))
                                @continue;
                            @endif
                            <div class="item @if($key == 1) active @endif" data-key="{{$key}}">{{$category->title}} <div class="deco"></div></div>
                        @endforeach
                    @endif
                </div>
                <div class="sliderControls">
                    <button class="backbtn"><span class="ic-left"></span></button>
                    <div class="counter"><span>1</span>/{{count($data['list'])}}</div>
                    <button class="forwardbtn"><span class="ic-right"></span></button>
                </div>
            </div>
        </div>
    </div>
</section>
