<div class="infoSection">
    <div class="container">
        <div class="advantages">
            @if(! empty($data['title']))
                <div class="caption">{{$data['title']}}</div>
            @endif
            @if(isset($data['list']) && count($data['list']))
                <div class="itemsWrp">
                    @foreach($data['list'] as $item)
                        @if(! empty($item['text']))
                            <div class="textItem">
                                <p>{{$item['text']}}</p>
                            </div>
                        @endif
                        @if(! empty($item['btn_text']))
                            <div class="textItem nodeco">
                                <a href="{{$item['btn_link']}}" class="lnk">{{$item['btn_text']}}</a>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>
