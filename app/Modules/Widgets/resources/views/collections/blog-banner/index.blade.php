<?php
$articles = \App\Models\BlogArticles::query()->active()->get();
$currentLang = app()->getLocale();
?>

<div class="blogSloderBlock">
    @if(isset($data['list']) && count($data['list']))
    <div>
        <div class="blogSlider">
            @foreach($data['list'] as $key => $item)
                @foreach($articles as $article)
                    @if($item['article_id'] == $article->id)
                        <?php
                        $url = '/blog/' . $article->slug;

                        if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                            $url = '/' . $currentLang . $url;
                        }
                        ?>
                        <div class="slide">
                            <img class="slideImg" src="{{get_image_uri($article->image,'original')}}" alt="">
                            <div class="botSlide">
                                <div class="caption">{{$article->getTranslation(app()->getLocale())->name}}
                                </div>
                                <a href="{{$url}}">{{__('Read article')}}</a>
                                <div class="controls">
                                    <button class="backbtn"><span class="ic-left"></span></button>
                                    <div class="counter"><span>{{$key}}</span>/{{count($data['list'])}}</div>
                                    <button class="forwardbtn"><span class="ic-right"></span></button>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endforeach
        </div>
    </div>
    @endif
    <div>
        <form action="{{route('front.subscribe')}}"  method="post" class="subscribe" id="subscribe_form">
            @csrf
            <div class="caption">Получай новости первым!</div>
            <div class="subCaption">Подпишись на рассылку новостей и получай рассылку самых свежих новостей
                на електронную почту
            </div>
            <input type="email" name="email" placeholder="Email">
            <label style="display: none; color: red" class="error-email-subs">{{__('Enter your email')}}</label>
            <button type="submit" class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_SITE_KEY')}}" data-callback="onSubmit">Подписаться на рассылку</button>
        </form>
    </div>
</div>
