<?php
$articles = \App\Models\BlogArticles::query()->active()->get();
$currentLang = app()->getLocale();
?>
<section class="infoSection">
    <div class="container">
        <div class="showcaseBlock">
            @if(! empty($data['title']))
                <h2 class="caption">{{$data['title']}}</h2>
            @endif
            @if(! empty($data['subtitle']))
                <p class="subtext">{{$data['subtitle']}}</p>
            @endif
            @if(isset($data['list']) && count($data['list']))
                <div class="itemsWrp">
                    @foreach($data['list'] as $item)
                        @foreach($articles as $article)
                            @if($item['article_id'] == $article->id)
                                <?php
                                $url = '/blog/' . $article->slug;

                                if($currentLang !== \App\Models\Langs::getDefaultLangCode()){
                                    $url = '/' . $currentLang . $url;
                                }
                                ?>
                                <div class="itemWrp">
                                    <a href="{{$url}}">
                                        <img src="{{get_image_uri($article->image,'original')}}" alt="">
                                        <div class="top"></div>
                                        <div class="bot">
                                            {{$article->getTranslation(app()->getLocale())->name}}
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    @endforeach
                </div>
            @endif
        </div>
        <div class="botlnk">
            <a href="{{route('blog')}}">{{$data['btn_name']}}</a>
        </div>
    </div>
</section>
