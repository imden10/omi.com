<div class="offerBlock">
    <p class="title">{{$data['title']}}</p>
    <div class="offerAdvantages">
        <div class="item">
            <img src="{{get_image_uri($data['image'],'original')}}" alt="">
            {!! $data['text'] !!}
        </div>
        <div class="item">
            <img src="{{get_image_uri($data['image2'],'original')}}" alt="">
            {!! $data['text2'] !!}
        </div>
    </div>
</div>
