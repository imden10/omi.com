<section class="productShowcase">
    <div class="container">
        @if(! empty($data['list']))
            @foreach($data['list'] as $list)
                <div class="showcaseBlock">
                    <h2 class="caption">{{$list['title']}}</h2>
                    <?php
                        $category1 = \App\Models\Category::query()->where('id',$list['category_id_1'])->first();

                        if(! $category1){
                            continue;
                        }

                        $trans1 = $category1->getTranslation(app()->getLocale());
                        $countProduct1 = $category1->countProductsInSubCategories();
                        $countSubCategories1 = $category1->countSubCategories();

                        $category2 = \App\Models\Category::query()->where('id',$list['category_id_2'])->first();

                        if(! $category2){
                            continue;
                        }

                        $trans2 = $category2->getTranslation(app()->getLocale());
                        $countProduct2 = $category2->countProductsInSubCategories();
                        $countSubCategories2 = $category2->countSubCategories();

                        $category3 = \App\Models\Category::query()->where('id',$list['category_id_3'])->first();

                        if(! $category3){
                            continue;
                        }

                        $trans3 = $category3->getTranslation(app()->getLocale());
                        $countProduct3 = $category3->countProductsInSubCategories();
                        $countSubCategories3 = $category3->countSubCategories();

                    ?>
                    <div class="itemsWrp">
                        <div class="itemWrp">
                            <a href="/catalog/{{$category1->path}}">
                                <img src="{{get_image_uri($category1->image, 'original')}}" alt="123">
                                <div class="top">
                                    <span>{{$countSubCategories1}} {{ trans_choice('category_choice',$countSubCategories1 )}}</span>
                                    <span>{{$countProduct1}} {{ trans_choice('product_choice',$countProduct1 )}}</span>
                                </div>
                                <div class="bot">{{$trans1->title}}</div>
                            </a>
                        </div>
                        <div class="itemWrp">
                            <a href="/catalog/{{$category2->path}}">
                                <img src="{{get_image_uri($category2->image, 'original')}}" alt="123">
                                <div class="top">
                                    <span>{{$countSubCategories2}} {{ trans_choice('category_choice',$countSubCategories2 )}}</span>
                                    <span>{{$countProduct2}} {{ trans_choice('product_choice',$countProduct2 )}}</span>
                                </div>
                                <div class="bot">{{$trans2->title}}</div>
                            </a>
                        </div>
                        <div class="itemWrp">
                            <a href="/catalog/{{$category3->path}}">
                                <img src="{{get_image_uri($category3->image, 'original')}}" alt="123">
                                <div class="top">
                                    <span>{{$countSubCategories3}} {{ trans_choice('category_choice',$countSubCategories3 )}}</span>
                                    <span>{{$countProduct3}} {{ trans_choice('product_choice',$countProduct3 )}}</span>
                                </div>
                                <div class="bot">{{$trans3->title}}</div>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</section>
