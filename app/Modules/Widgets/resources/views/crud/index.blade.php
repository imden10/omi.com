@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Віджети</li>
        </ol>
    </nav>

    <div class="card">
        <div class="card-header">
            <ul class="nav nav-pills" style="display: inline-flex">
                @foreach(\App\Models\Langs::getLangsWithTitleShort() as $langCode => $item)
                    <li class="nav-item">
                        <a class="nav-link @if(request()->get('lang') === $langCode || (!request()->get('lang') && \App\Models\Langs::getDefaultLangCode() === $langCode)) active @endif" aria-current="page" href="?lang={{$langCode}}">{{$item}}</a>
                    </li>
                @endforeach
            </ul>

            <form action="{{ route(config('widgets.route_name_prefix', 'admin.') . 'widgets.store') }}" method="post" style="display: inline-block">
                @csrf

                <input type="hidden" name="lang" value="{{ request('lang', app()->getLocale()) }}">

                <div class="form-inline">
                    <label for="widgetInstance" class="mr-3">Шаблон виджета <span class="text-danger">*</span></label>

                    <div>
                        <select id="widgetInstance" name="instance" class="form-control custom-select-sm @error('instance') is-invalid @enderror">
                            <option value="">---</option>
                            @foreach($list as $instance => $name)
                                <option value="{{ $instance }}" @if (old('instance') == $instance) selected @endif>{{ trans($name) }}</option>
                            @endforeach
                        </select>

                        @error('instance')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-sm btn-info ml-3">
                        <i class="far fa-plus-square"></i>
                        Создать
                    </button>
                </div>
            </form>
        </div>
    </div>

    @if ($widgets->isNotEmpty())
        <div class="card">
            <div class="card-body">
                <table class="table table-striped table-sm table-valign-middle">
                    <thead>
                        <tr>
                            <th class="text-center font-weight-bold" style="width: 40%">Название</th>
                            <th class="text-center font-weight-bold" style="width: 40%">Шаблон</th>
                            <th class="text-center font-weight-bold" style="width: 20%">Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($widgets as $widget)
                            <tr>
                                <td>
                                    <a href="{{ route(config('widgets.route_name_prefix', 'admin.') . 'widgets.edit', ['widget' => $widget, config('widgets.request_lang_key') => $widget->lang]) }}" title="Редактировать">
                                        {{ $widget->name }}
                                    </a>
                                </td>

                                <td class="text-center">
                                    <span class="text-gray font-weight-normal">
                                        {{ isset($list[$widget->instance]) ? $list[$widget->instance] : $widget->instance }}
                                    </span>
                                </td>

                                <td class="text-center">
                                    <form action="{{ route(config('widgets.route_name_prefix', 'admin.') . 'widgets.destroy', $widget) }}" method="post">
                                        @csrf

                                        @method('delete')

                                        @can('widgets.update')
                                            <a href="{{ route(config('widgets.route_name_prefix', 'admin.') . 'widgets.edit', ['widget' => $widget, config('widgets.request_lang_key') => $widget->lang]) }}" class="btn btn-sm btn-success  text-white" title="Редактировать">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                        @endcan

                                        @can('widgets.delete')
                                            <button type="submit" class="btn btn-sm btn-danger delete-item text-white" title="Удалить">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        @endcan
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection
