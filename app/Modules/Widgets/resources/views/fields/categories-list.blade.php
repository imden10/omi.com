<?php
$categories = app(\App\Models\Category::class)->treeStructure();
?>

<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                <div class="col-lg-5 mb-3">
                    <label>Категория</label>
                    <select class="select2-field category-select" name="{{ $field['name'] }}[#dynamicListPlaceholder][category_id]" style="width: 100%">
                        @include('components.category_node',['sel' => 0])
                    </select>
                </div>

                <div class="col-lg-5 mb-3">
                    <label>Продукт</label>
                    <select class="select2-field product-select" disabled name="{{ $field['name'] }}[#dynamicListPlaceholder][product_id]" style="width: 100%">
                        <option value="0">---</option>
                    </select>
                </div>

                <div class="col-lg-2">
                    <button type="button" class="btn btn-danger remove-item float-right text-white">Видалити</button>
                </div>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">

                    <div class="col-lg-5 mb-3">
                        <label>Категория</label>
                        <select class="select2-field-shown category-select" name="{{ $field['name'] }}[{{ $key }}][category_id]" style="width: 100%">
                            @include('components.category_node',['sel' => old($field['name'] . '.' . $key . '.category_id', $value['category_id'] ?? 0)])
                        </select>
                    </div>

                    <div class="col-lg-5 mb-3">
                        <label>Продукт</label>
                        <select class="select2-field-shown product-select" name="{{ $field['name'] }}[{{ $key }}][product_id]" style="width: 100%">
                            <?php
                                $category = \App\Models\Category::query()->where('id',old($field['name'] . '.' . $key . '.category_id', $value['category_id'] ?? 0))->first();
                            ?>
                            <option value="0">---</option>
                            @if($category && $category->productsInSubCategories())
                                @foreach($category->productsInSubCategories() as $item)
                                        <option value="{{$item->id}}" @if($item->id == old($field['name'] . '.' . $key . '.product_id', $value['product_id'] ?? 0)) selected @endif>{{$item->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="col-lg-2">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Видалити</button>
                    </div>

                    @error($field['name'] . '.' . $key)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Додати</button>
</div>

@push('styles')
    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           $('.select2-field-shown').each(function () {
                $(this).select2({});
            });

           $(document).on('change', ".category-select", function () {
               let _this = $(this);
               let category_id = _this.val();
               $.ajax({
                   url:"{{route('widget.get-products-by-category')}}",
                   type:"post",
                   data:{
                       _token:"{{csrf_token()}}",
                       category_id:category_id
                   },
                   success:function(data){
                       _this.closest('.input-group').find('.product-select').html("");
                       _this.closest('.input-group').find('.product-select').append(data);
                       _this.closest('.input-group').find('.product-select').removeAttr('disabled');
                       _this.closest('.input-group').find('.product-select').select2({});
                   }
               });
           })
        });

        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('summernote')) {
                    $(this).summernote(summernote_options);
                }
            });

            container.find('.select2-field').each(function () {
                $(this).select2({});
            });
        });

        $('.items-container').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
