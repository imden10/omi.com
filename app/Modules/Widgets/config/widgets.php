<?php

return [

    /*
    |------------------------------------------------------------------
    | View layout component
    |------------------------------------------------------------------
    */
    //    'view-layout' => 'admin::layout',
    'view-layout'  => 'admin::layout',

    /*
    |------------------------------------------------------------------
    | View layout title
    |------------------------------------------------------------------
    */
    'layout-title' => 'widgets::strings.layout.title',


    'widgets'           => [
        'main-banner'       => \App\Modules\Widgets\Collections\MainBanner\MainBannerWidget::class,
        'production'        => \App\Modules\Widgets\Collections\Production\ProductionWidget::class,
        'category-slider'   => \App\Modules\Widgets\Collections\CategorySlider\CategorySliderWidget::class,
        'we-offer'          => \App\Modules\Widgets\Collections\WeOffer\WeOfferWidget::class,
        'we-offer-for-main' => \App\Modules\Widgets\Collections\WeOfferForMain\WeOfferForMainWidget::class,
        'services'          => \App\Modules\Widgets\Collections\Services\ServicesWidget::class,
        'blog'              => \App\Modules\Widgets\Collections\Blog\BlogWidget::class,
        'subscribe'         => \App\Modules\Widgets\Collections\Subscribe\SubscribeWidget::class,
        'omi-logo'          => \App\Modules\Widgets\Collections\OmiLogo\OmiLogoWidget::class,
        'image-and-text'    => \App\Modules\Widgets\Collections\ImageAndText\ImageAndTextWidget::class,
        'advantages'        => \App\Modules\Widgets\Collections\Advantages\AdvantagesWidget::class,
        'faq-and-feedback'  => \App\Modules\Widgets\Collections\FaqAndFeedback\FaqAndFeedbackWidget::class,
        'stock'             => \App\Modules\Widgets\Collections\Stock\StockWidget::class,
        'blog-banner'       => \App\Modules\Widgets\Collections\BlogBanner\BlogBannerWidget::class,
    ],

    /*
    |------------------------------------------------------------------
    | Permissions for manipulation widgets
    |------------------------------------------------------------------
    */
    'permissions'       => [
        'widgets.view'   => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.create' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.update' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.delete' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
    ],

    /*
    |------------------------------------------------------------------
    | Middleware for settings
    |------------------------------------------------------------------
    */
    'middleware'        => ['web', 'auth', 'verified'],

    /*
    |------------------------------------------------------------------
    | Uri Route prefix
    |------------------------------------------------------------------
    */
    'uri_prefix'        => 'admin',

    /*
    |------------------------------------------------------------------
    | Route name prefix
    |------------------------------------------------------------------
    */
    'route_name_prefix' => 'admin.',

    /*
    |------------------------------------------------------------------
    | Request lang key
    |------------------------------------------------------------------
    */
    'request_lang_key'  => 'lang',

];
