<?php

namespace App\Modules\Widgets\Collections\Production;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class ProductionWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Продукция OMI TRADE';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.production.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'production-list',
                'name' => 'list',
                'label' => 'Продукцция',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
