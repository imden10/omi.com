<?php

namespace App\Modules\Widgets\Collections\Subscribe;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class SubscribeWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Подписка';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.subscribe.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'text',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:1000',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'btn_name',
                'label' => 'Надпись на кнопке',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
