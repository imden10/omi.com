<?php

namespace App\Modules\Widgets\Collections\BlogBanner;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class BlogBannerWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Блог главный слайдер';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.blog-banner.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'blog-list',
                'name' => 'list',
                'label' => 'Публикации',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
