<?php

namespace App\Modules\Widgets\Collections\Stock;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class StockWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Акция (под продуктом)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.stock.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'image',
                'name' => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'editor',
                'name' => 'text',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:1000',
                'value' => '',
            ],
            [
                'type' => 'image',
                'name' => 'image2',
                'label' => 'Изображение 2',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'editor',
                'name' => 'text2',
                'label' => 'Текст 2',
                'class' => '',
                'rules' => 'nullable|string|max:1000',
                'value' => '',
            ],
        ];
    }
}
