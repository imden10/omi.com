<?php

namespace App\Modules\Widgets\Collections\ImageAndText;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class ImageAndTextWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Изображение и текст';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.image-and-text.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'editor',
                'name' => 'text',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:1000',
                'value' => '',
            ],
            [
                'type' => 'select2',
                'name' => 'image_position',
                'label' => 'Позиция изображения',
                'class' => '',
                'rules' => 'nullable|string',
                'value' => null,
                'list' => function(){
                    return [
                        'left' =>'left',
                        'right' =>'right'
                    ];
                }
            ],
            [
                'type' => 'image',
                'name' => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
