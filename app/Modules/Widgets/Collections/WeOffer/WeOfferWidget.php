<?php

namespace App\Modules\Widgets\Collections\WeOffer;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class WeOfferWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Мы предлагаем';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.we-offer.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'image-and-text-list',
                'name' => 'list',
                'label' => 'Пункты',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
