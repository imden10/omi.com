<?php

namespace App\Modules\Widgets\Collections\CategorySlider;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class CategorySliderWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Выбирай лучшее (слайдер категорий)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.category-slider.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Слайды'],
            [
                'type' => 'categories-list',
                'name' => 'list',
                'label' => 'Слайды',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
