<?php

namespace App\Modules\Widgets\Collections\FaqAndFeedback;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class FaqAndFeedbackWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'FAQ и форма обратной связи';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.faq-and-feedback.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'form_title',
                'label' => 'Заголовок формы',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'form_sub_title',
                'label' => 'Подзаголовок формы',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'form_btn_text',
                'label' => 'Текст на кнопке формы',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'faq-list',
                'name' => 'list',
                'label' => 'FAQ',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
