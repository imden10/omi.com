<?php

namespace App\Modules\Widgets\Collections\WeOfferForMain;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class WeOfferForMainWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Преимущества для главной';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.we-offer-for-main.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select2',
                'name'  => 'columns',
                'label' => 'Количество колонок',
                'class' => '',
                'rules' => 'nullable|string',
                'value' => 3,
                'list'  => function () {
                    return [
                        ''          => 3,
                        'fourItems' => 4
                    ];
                }
            ],
            [
                'type'  => 'image-and-text-list',
                'name'  => 'list',
                'label' => 'Пункты',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
