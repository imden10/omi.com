<?php

namespace App\Modules\Widgets\Collections\OmiLogo;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class OmiLogoWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Omi логотип';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.omi-logo.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [

        ];
    }
}
