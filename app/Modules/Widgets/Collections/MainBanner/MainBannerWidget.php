<?php

namespace App\Modules\Widgets\Collections\MainBanner;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class MainBannerWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Главный баннер';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.main-banner.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'subtitle2',
                'label' => 'Подзаголовок 2',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Текст в две колонки'],
            [
                'type' => 'text',
                'name' => 'column_title1',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'column_subtitle1',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'column_title2',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'column_subtitle2',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'btn_name',
                'label' => 'Текст на кнопке',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'after_btn_text',
                'label' => 'Текст после кнопки',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'image',
                'name' => 'image',
                'label' => 'Фон',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
