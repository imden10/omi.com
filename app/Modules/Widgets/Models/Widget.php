<?php

namespace App\Modules\Widgets\Models;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    /**
     * @var string
     */
    protected $table = 'widgets';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'instance',
        'data',
        'lang',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];
}
