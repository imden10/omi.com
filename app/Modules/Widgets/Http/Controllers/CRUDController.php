<?php

namespace App\Modules\Widgets\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Widgets\Http\Requests\WidgetRequest;
use App\Modules\Widgets\Widget;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Http\Request;

class CRUDController extends Controller
{
    /**
     * @var Widget
     */
    private Widget $widget;

    /**
     * WidgetController constructor.
     *
     * @param Widget $widget
     */
    public function __construct(Widget $widget)
    {
        $this->widget = $widget;
    }

    /**
     * Output widgets list
     *
     * @param Request $request
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('widgets.view');

        $lang = $request->input(config('widgets.request_lang_key'), app()->getLocale());

        $widgets = $this->widget->index($lang);

        $list = $this->widget->list();

        return view('widgets::crud.index', compact('widgets', 'list'));
    }

    /**
     * Store widget
     *
     * @param WidgetRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(WidgetRequest $request)
    {
        $this->authorize('widgets.create');

        $widget = $this->widget->store($request->all());

        return $widget
            ? redirect()
                ->route(config('widgets.route_name_prefix', 'admin.') . 'widgets.edit', ['widget' => $widget, config('widgets.request_lang_key') => $widget->lang])
                ->with('notification.success', trans('widgets::strings.notifications.save.success'))
            : redirect()
                ->back()
                ->withInput()
                ->with('notification.error', trans('widgets::strings.notifications.save.error'));
    }

    /**
     * Edit widget
     *
     * @param $id
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $this->authorize('widgets.update');

        $widget = $this->widget->edit($id);

        $object = $this->widget->object($widget->instance);

        if (!$object) {
            abort(404);
        }

        return view('widgets::crud.edit', compact('object', 'widget'));
    }

    /**
     * Update widget
     *
     * @param WidgetRequest $request
     * @param $id
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(WidgetRequest $request, $id)
    {
        $this->authorize('widgets.update');

        $widget = $this->widget->edit($id);

        $fields = $this->widget->fields($widget->instance);

        $request->validate($fields);

        $data = array_merge(
            $request->except(array_keys($fields)),
            ['data' => $request->only(array_keys($fields))]
        );

        return $this->widget->update($data, $id)
            ? redirect()
                ->back()
//                ->route(config('widgets.route_name_prefix', 'admin.') . 'widgets.edit', $id)
                ->with('notification.success', trans('widgets::strings.notifications.update.success'))
            : redirect()
                ->back()
                ->withInput()
                ->with('notification.error', trans('widgets::strings.notifications.update.error'));
    }

    /**
     * Delete widget
     *
     * @param $id
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy($id)
    {
        $this->authorize('widgets.delete');

        return $this->widget->destroy($id)
            ? redirect()
                ->route(config('widgets.route_name_prefix', 'admin.') . 'widgets.index')
                ->with('notification.success', trans('widgets::strings.notifications.delete.success'))
            : redirect()
                ->route(config('widgets.route_name_prefix', 'admin.') . 'widgets.index')
                ->with('notification.success', trans('widgets::strings.notifications.delete.error'));
    }
}
