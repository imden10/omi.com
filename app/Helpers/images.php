<?php

if (!function_exists('get_image_uri')) {

    function get_image_uri($path, $template = 'original', $absolute = true) {

        try {
            if (!empty($path) && in_array(pathinfo($path)['extension'], ['pdf'])) {
                return '/images/pdf_placeholder.jpg';
            }
        } catch (Exception $e){
//            dd($path,pathinfo($path),$e->getMessage());
        }


//        return $path;
        $file = '/images/no-image.png';

        if (!empty($path) && in_array(pathinfo($path)['extension'], ['mp4', 'm4v', '3gp'])) {
            $file = '/images/video-placeholder.png';
        } elseif (!empty($path)) {
            $file = $path;
        }

        if (!empty($path) && pathinfo($path)['extension'] == 'svg') {
            return asset('storage/media/' . trim($file, '/'));
        }

        return route('imagecache', ['template' => $template, 'filename' => trim($file, '/')], $absolute);
    }

}
