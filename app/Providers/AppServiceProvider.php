<?php

namespace App\Providers;

use App\Models\Langs;
use App\Modules\Setting\Setting;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Setting::class, function ($app) {
            return new Setting();
        });
        $this->app->alias(Setting::class, 'Setting');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('admin', function() {
            return auth()->check() && auth()->user()->isAdmin();
        });

        Blade::if('user', function() {
            return auth()->check() && auth()->user()->isUser();
        });

        $this->includeHelpers();

        Paginator::useBootstrap();

        config(['translatable.locales' => Langs::getLangsWithTitle()]);
        config(['translatable.locale' => Langs::getDefaultLangCode()]);
        config(['translatable.fallback_locale' => Langs::getDefaultLangCode()]);
    }

    /**
     * Include helper functions
     *
     * @return void
     */
    protected function includeHelpers(): void
    {
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $file) {
            require_once $file;
        }
    }
}
