<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class ProfileConfirm
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->isUser()) {
            if (is_null(Auth::user()->email_verified_at)) {
                return redirect()->route('auth.email-confirm');
            } elseif (is_null(Auth::user()->phone_verified_at)) {
                return redirect()->route('auth.phone-confirm');
            }

            return $next($request);
        } else {
            abort(404);
        }
    }
}
