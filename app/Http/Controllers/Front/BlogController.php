<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\BlogTags;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $model = BlogArticles::query()
            ->orderBy('created_at', 'desc')
            ->active()
            ->paginate(9);

        return view('front.blog.index', [
            'model' => $model
        ]);
    }

    public function tags($slug)
    {
        $tagArticleIds = BlogTags::query()
            ->leftJoin('blog_article_tag', 'blog_article_tag.blog_tag_id', '=', 'blog_tags.id')
            ->where('blog_tags.slug', $slug)
            ->select([
                'blog_article_tag.blog_article_id AS article_id'
            ])
            ->pluck('article_id')
            ->toArray();

        $tag = BlogTags::query()->where('slug', $slug)->first();

        if (count($tagArticleIds) == 0)
            abort(404);

        $model = BlogArticles::query()
            ->whereIn('id', $tagArticleIds)
            ->orderBy('created_at', 'desc')
            ->active()
            ->paginate(9);

        return view('front.blog.index', [
            'model'       => $model,
            'currentSlug' => $tag->slug
        ]);
    }

    public function single($slug)
    {
        $model = BlogArticles::query()
            ->where('slug', $slug)
            ->active()
            ->first();

        if (!$model)
            abort(404);

        return view('front.blog.single', [
            'page' => $model
        ]);
    }
}
