<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Products;

class CategoryController extends Controller
{
    public function show($slugString, $filterString = null)
    {
        $last        = '';
        $cat_filters = request()->get('filter', []);

        //        dd($cat_filters);

        if ($filterString) {
            $levels = explode('/', $filterString);
            foreach ($levels as $lvl) {
                $last = $lvl;
            }

            if ($last && Category::where('slug', '=', $last)->exists()) {//sub category, no filter
                $slugString = $last;
            } else {
                abort(404);
            }
        }

        $cat = Category::FindBySlugOrFail($slugString);

        $categories = Category::where('parent_id', '=', $cat->id)
            ->active()->orderBy('order')->get()
            ->map(function ($item, $key) {
                $item->products_count = $item->countProductsInSubCategories();
                $item->title          = $item->translateOrDefault(app()->getLocale())->title;
                return $item;
            });

        $cat_info       = $cat->translateOrDefault(app()->getLocale());
        $cat_info->slug = $cat->path;

        $cat_info->productsCount = $cat->countProductsInSubCategories();
        $cat_info->activePattern = 0;

        if ($cat->parent_id == 0) {

            return view('front.category.main', [
                'model'    => $cat,
                'cat_info' => $cat_info,
                'children' => $categories
            ]);
        }

        //breadcrumbs
        $breadcrumbs  = [];
        $bread_slises = explode('/', $cat->path);
        //dd($bread_slises);
        if ($bread_slises) {
            foreach ($bread_slises as $slice) {
                $one_slice      = Category::FindBySlugOrFail($slice);
                $one_slice_info = $one_slice->translateOrDefault(app()->getLocale());

                $breadcrumbs[] = [
                    'title' => $one_slice_info->title,
                    'href'  => route('category.show', [$one_slice->path])
                ];
            }
        }

        $cat_attr = $cat->attributes->keyBy('id')->toArray();

        if(count($cat_attr)){
            foreach ($cat_attr as $key => $attr){
                foreach ($attr['translations'] as $trans){
                    if($trans['lang'] == app()->getLocale()){
                        $cat_attr[$key]['name'] = $trans['name'];
                    }
                }
            }
        }

        $attr     = [];

        $products = Products::query()->whereHas('categories', function ($q) use ($slugString, $categories) {
            $q->where('slug', '=', $slugString);
            $q->orWhereIn('category.id', $categories->pluck('id')->all());
        })->active();//status == 1

        if (count($cat_filters)) {
            foreach ($cat_filters as $filter_attribute_id => $filter) {
                foreach ($filter as $filter_slug) {
                    $products->whereHas('attributes', function ($q) use ($filter_attribute_id, $filter_slug) {
                        $q->where('slug', '=', $filter_slug)
                            ->where('attribute_id', '=', $filter_attribute_id);
                    });
                }
            }
        }

        $countProducts = $products->count();

        $products = $products->with('attributes')->orderByDesc('id');

        $new_products = clone $products;

        $products = $products->get();

        foreach ($products as $product_key => $product) {
            $product_attributes = $product->attributes->keyBy('id')->toArray();//array of attributes_translations

            if ($product_attributes) {

                foreach ($product_attributes as $attribute) {
                    if (is_null($attribute['value']) || empty($attribute['value'])) {
                        continue;
                    }

                    $key = $attribute['attribute_id'];

                    if (isset($cat_attr[$key])) {
                        if (!isset($attr[$key])) {
                            $attr[$key]['count'] = 0;
                        }

                        if (!isset($attr[$key]['variants'][$attribute['slug']])) {
                            $attr[$key]['variants'][$attribute['slug']]['count'] = 1;

                            if (isset($cat_filters[$key]) && in_array($attribute['slug'], $cat_filters[$key])) {//active filter
                                $attr[$key]['variants'][$attribute['slug']]['active'] = true;
                            } else {
                                $attr[$key]['variants'][$attribute['slug']]['active'] = false;
                            }

                            $attr[$key]['variants'][$attribute['slug']]['name'] = $attribute['value'];
                            $d = preg_match("/[0-9]+(\,[0-9][0-9]?)?/",$attribute['value'],$d1);

                            if(isset($d1[0])){
                                $d1[0] = str_replace(',','.',$d1[0]);
                            }

                            $attr[$key]['variants'][$attribute['slug']]['n'] = $d ? ((float)$d1[0]) : $attribute['value'];
                            $attr[$key]['count']++;
                        } else {
                            $attr[$key]['variants'][$attribute['slug']]['count']++;
                            $attr[$key]['count']++;
                        }
                    }
                }
            }
        }

        $products = $new_products->paginate(16);

        foreach ($products as $product_key => $product) {
            $product->name = $product->translateOrDefault(app()->getLocale())->name;
        }

        if(count($attr)){
            foreach ($attr as $key => $item){
                uasort($attr[$key]['variants'], array('App\Service\Sort','cmp'));
            }
        }

        if(count($cat_attr)){
            uasort($cat_attr, array('App\Service\Sort','cmp2'));
        }

        return view('front.category.sub', [
            'model'         => $cat,
            'cat_info'      => $cat_info,
            'children'      => $categories,
            'breadcrumbs'   => $breadcrumbs,
            'products'      => $products,
            'filters'       => $cat_attr,
            'variants'      => $attr,
            'countProducts' => $countProducts
        ]);
    }
}
