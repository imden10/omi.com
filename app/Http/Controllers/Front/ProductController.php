<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Orders;
use App\Models\Products;
use App\Service\TelegramBot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    private $bot;

    public function __construct(TelegramBot $telegramBot)
    {
        $this->bot = $telegramBot;
    }

    public function show($path = null, $slugString)
    {
        /* @var $product Products */
        $product = Products::FindBySlugOrFail($slugString);

        if ($product->status == 0) {
            abort(404);
        }

        //breadcrumbs
        $breadcrumbs = array();
        $cat_path    = ''; //all categories
        $last_cat    = ''; //last category, for related products

        $cat_path = $product->categories->first()->path;

        // dd($cat_path);
        $bread_slises = explode('/', $cat_path);
        // dd($bread_slises);

        if ($bread_slises) {
            foreach ($bread_slises as $slice) {
                $one_slice      = Category::FindBySlugOrFail($slice);
                $one_slice_info = $one_slice->translateOrDefault(app()->getLocale());

                $breadcrumbs[] = array(
                    'title' => $one_slice_info->title,
                    'href'  => route('category.show', [$one_slice->path])
                );

                $last_cat = $slice;
            }
        }

        $prod_trans = $product->translateOrDefault(app()->getLocale());

        $breadcrumbs[] = array(
            'title' => $prod_trans->name,
            'href'  => ''
        );

        //        dd($product,$prod_trans,$breadcrumbs);

        return view('front.product.index', [
            'product'     => $product,
            'prod_trans'  => $prod_trans,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    public function addRequest(Request $request)
    {
        $recaptcha_secret = env('GOOGLE_RECAPTCHA_SECRET_KEY');

        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $recaptcha_secret . "&response=" . $request->get('g-recaptcha-response') . "&remoteip=" . $_SERVER['REMOTE_ADDR']);

        $response = json_decode($response, true);

        if ($response["success"] == true || $request->has('no_recaptcha')) {

            $product = Products::query()->where('id', $request->get('product_id'))->first();

            $message = "Оформление заказа" . PHP_EOL;

            $message .= "Имя - " . $request->get('name') . PHP_EOL;
            $message .= "Электронная почта - " . $request->get('email') . PHP_EOL;
            $message .= "Номер телефона - " . $request->get('phone') . PHP_EOL;
            $message .= "Продукт - " . $product->name . PHP_EOL;

            $flag = $this->bot->sendMessage($message);

            $order = Orders::create([
                'name'       => $request->get('name'),
                'phone'      => $request->get('phone'),
                'email'      => $request->get('email'),
                'product_id' => $request->get('product_id'),
                'status'     => Orders::STATUS_NEW
            ]);

            return [
                'success' => true
            ];

        } else {
            Log::info('no recaptcha');
        }
    }
}
