<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\Services;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function show($slug)
    {
        $page = Services::query()
            ->where('slug',$slug)
            ->where('status',Services::STATUS_ACTIVE)
            ->first();

        if(! $page) return abort(404);

        return view('front.services.index',[
            'page' => $page,
        ]);
    }
}
