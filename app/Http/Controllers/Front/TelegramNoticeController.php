<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\TelegramNoticeRequest;
use App\Service\TelegramBot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TelegramNoticeController extends Controller
{
    private $bot;

    public function __construct(TelegramBot $telegramBot)
    {
        $this->bot = $telegramBot;
    }

    public function addNotice(TelegramNoticeRequest $request)
    {
        $recaptcha_secret = env('GOOGLE_RECAPTCHA_SECRET_KEY');

        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$request->get('g-recaptcha-response')."&remoteip=".$_SERVER['REMOTE_ADDR']);


        $response = json_decode($response, true);

        if($response["success"] == true || $request->has('no_recaptcha')) {
            $message = "Новое сообщение" . PHP_EOL;

            if(! empty($request->get('name'))){
                $message .= "Имя - " . $request->get('name') . PHP_EOL;
            }

            if(! empty($request->get('email'))){
                $message .= "Электронная почта - " . $request->get('email') . PHP_EOL;
            }

            if(! empty($request->get('phone'))){
                $message .= "Номер телефона - " . $request->get('phone') . PHP_EOL;
            }

            if(! empty($request->get('service_name'))){
                $message .= "Услуга - " . $request->get('service_name') . PHP_EOL;
            }

            $flag = $this->bot->sendMessage($message);

            if(is_array($flag) && array_key_exists('ok',$flag) && $flag['ok']){
                return [
                    'success' => true
                ];
            }
        } else {
            Log::info('no recaptcha');
        }

        return [
            'success' => false
        ];
    }
}
