<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Pages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PageController extends Controller
{
    public function index($slug = null)
    {
        if(! $slug) $slug = '/';

        $page = Pages::query()
            ->where('slug',$slug)
            ->first();

        if(! $page) return abort(404);

        return view('front.site.index',[
            'page' => $page,
        ]);
    }
}
