<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogCategories;
use App\Models\Menu;
use Illuminate\Http\Request;

class BlogCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = BlogCategories::query()->defaultOrder()->get()->toTree();

        return view('admin.blog.categories.index',[
            'model' => json_encode($model, JSON_UNESCAPED_UNICODE),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new BlogCategories();

        return view('admin.blog.categories.form', [
            'model' => $model
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();

        BlogCategories::create($post);

        return redirect()->route('categories.index')->with('success', 'Сторінка успішно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = BlogCategories::query()->where('id',$id)->first();

        return view('admin.blog.categories.form', [
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* @var $model BlogCategories */
        $model = BlogCategories::query()->where('id', $id)->first();

        $post = $request->all();

        $model->fill($post);
        $model->save();

        return redirect()->route('categories.index')->with('success', 'Сторінка успішно оновлена');
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */
    public function destroy(Request $request, $id)
    {
        $confirm = $request->get('c') ?? 1;

        $node = BlogCategories::query()->where('id', $id)->with('children')->first();

        if ($confirm && count($node->children)) {
            return [
                'status'      => false,
                'hasChildren' => true,
                'message'     => 'Ця категорія містить в собі інші категорії, які теж будуть видалені!'
            ];
        }

        $node->delete();

        $tree = BlogCategories::query()->defaultOrder()->get()->toTree();

        return [
            'status'  => true,
            'tree'    => $tree,
            'message' => 'Категорію успішно видалено!'
        ];
    }

    public function rebuild(Request $request)
    {
        $isRebuild = BlogCategories::rebuildTree($request->get('tree'), true);

        if ($isRebuild) {
            return [
                'status'  => true,
                'message' => 'Категорію успішно оновлено!'
            ];
        }
    }
}
