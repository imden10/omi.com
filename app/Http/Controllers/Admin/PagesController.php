<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use Illuminate\Http\Request;
use App\Http\Requests\PagesRequest;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');

        $model = Pages::query()
            ->orderBy('created_at', 'desc')
            ->where(function ($q) use ($status) {
                if ($status != '') {
                    $q->where('status', $status);
                }
            })
            ->paginate(20);

        return view('admin.pages.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Pages();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.pages.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * @param PagesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Pages();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []),$constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('pages.index')->with('success', 'Страница успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pages $page)
    {
        $localizations = Langs::getLangsWithTitle();

        return view('admin.pages.edit', [
            'model'         => $page,
            'data'          => $page->getTranslationsArray(),
            'localizations' => $localizations
        ]);
    }

    /**
     * @param PagesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /* @var $model Pages */
        $model = Pages::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (! $model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []),$constructorData[$lang] ?? []));

                if (! $model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Страница успешно обновлена!');
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Pages $page)
    {
        $page->deleteTranslations();
        $page->delete();

        return redirect()->back()->with('success', 'Страницу успешно удалено!');
    }
}
