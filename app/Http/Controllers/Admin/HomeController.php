<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    public function login(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('admin.auth.login');
        } else {
            if (Auth::attempt([
                'email'    => $request->get('email'),
                'password' => $request->get('password'),
                'role_id'  => 1
            ])) {
                return redirect()->route('admin');
            }
            else {
                return redirect()->back()->with('error','Email or password is not correct!');
            }
        }
    }

    public function widgetGetProductsByCategory(Request $request)
    {
        /* @var $category Category */
        $category = Category::query()->where('id',$request->get('category_id'))->first();

        $options = '<option value="0">---</option>';

        foreach ($category->productsInSubCategories() as $item){
            $options .= '<option value="'.$item->id.'">'.$item->name.'</option>';
        }

        return $options;
    }
}
