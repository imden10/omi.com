<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Langs;
use App\Models\ProductAttributes;
use App\Models\ProductPrices;
use App\Models\Products;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ProductsController extends Controller
{
    /**
     * @var Category
     */
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $name = $request->get('name');

        $model = Products::query()
            ->leftJoin('product_translations', 'product_translations.products_id', '=', 'products.id')
            ->where('product_translations.lang',Langs::getDefaultLangCode())
            ->select([
              'products.*',
              'product_translations.name'
            ])
            ->orderBy('products.created_at', 'desc')
            ->where(function ($q) use ($status,$name) {
                if ($status != '') {
                    $q->where('products.status', $status);
                }
                if ($name != '') {
                    $q->where('product_translations.name', 'like', '%' . $name . '%');
                }
            })
            ->paginate(50);

        return view('admin.products.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Products();

        $model->order = 0;

        $localizations = Langs::getLangsWithTitle();

        $categories = $this->category->treeStructure();

        return view('admin.products.create', [
            'model'         => $model,
            'localizations' => $localizations,
            'categories'    => $categories,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Products();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Products::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.name'))));
            }

            $model->status = $request->get('status') ?? false;
            $model->is_stock_widget = $request->get('is_stock_widget') ?? false;

            $recommendationIds = [];

            if($request->get('categories')){
                $catIds = $request->get('categories');
                $categories = Category::query()
                   ->whereIn('id',$catIds)
                   ->get();

               foreach ($categories as $category){
                   if(count($recommendationIds) >= 4) break;
                   $products = $category->productsInSubCategories();

                   if($products){
                       foreach ($products as $product){
                           if(count($recommendationIds) >= 4) break;
                           if($product->id != $model->id && !in_array($product->id,$recommendationIds)){
                               $recommendationIds[] = $product->id;
                           }
                       }
                   }
               }
            }

            $model->recommendations = json_encode($recommendationIds);

            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->categories()->sync($request->get('categories'));

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []),$constructorData[$lang] ?? []));

//                $model->translateOrNew($lang)->fill($request->input('page_data.' . $lang, []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('products.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('products.edit', $model->id)->with('success', 'Продукт успешно добавлен!');
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Products::query()->where('id', $id)->first();

        $localizations = Langs::getLangsWithTitle();

        $categories = $this->category->treeStructure();

// \App\Models\ProductAttributes::query()->where('lang','ru')->where('product_id',$model->id)->delete();

        // dd(\App\Models\ProductAttributes::query()->where('lang','ru')->where('product_id',$model->id)->get());


        return view('admin.products.edit', [
            'model'         => $model,
            'data'          => $model->getTranslationsArray(),
            'localizations' => $localizations,
            'categories'    => $categories,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /* @var $model Products */
        $model = Products::query()->where('id', $id)->first();

            /* if has attr */
            $isDeleted = ProductAttributes::query()->where('product_id',$model->id)->delete();

// dd($isDeleted);

        DB::beginTransaction();

        try {
            $model->status = $request->get('status') ?? false;
            $model->is_stock_widget = $request->get('is_stock_widget') ?? false;

            $model->fill($request->all());

            if (! $model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            $model->categories()->sync($request->get('categories'));

            $attributes = $request->get('attr');


            // dd($isDeleted,$attributes);

            if(is_array($attributes) && count($attributes)){
                foreach (Langs::getLangsWithTitle() as $lang => $item) {
                    foreach ($attributes[$lang]['id'] as $key => $attrId){
                        if(! empty($attributes[$lang]['val'][$key])){
                            $productAttr = new ProductAttributes();
                            $productAttr->product_id = $model->id;
                            $productAttr->attribute_id = $attrId;
                            $productAttr->value = $attributes[$lang]['val'][$key];
                            $productAttr->slug = \Illuminate\Support\Str::slug($productAttr->value,'-');
                            $productAttr->lang = $lang;

                            if(! $productAttr->save()){
                                DB::rollBack();
                            }
                        }
                    }
                }
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []),$constructorData[$lang] ?? []));

//                $model->translateOrNew($lang)->fill($request->input('page_data.' . $lang, []));

                if (! $model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('products.edit', $model->id)->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Продукт успешно обновлен!');
    }

    /**
     * @param Products $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Products $product)
    {
        $product->deleteTranslations();
        $product->categories()->sync([]);

        ProductAttributes::query()->where('product_id',$product->id)->delete();
        ProductPrices::query()->where('product_id',$product->id)->delete();

        $product->delete();

        return redirect()->back()->with('success', 'Продукт успешно удалено!');
    }

    public function addPrice(Request $request)
    {
        $priceModel = new ProductPrices();
        $priceModel->product_id = $request->get('product_id');
        $priceModel->status = ProductPrices::STATUS_ACTIVE;
        $priceModel->order = 0;
        $priceModel->save();

        $elem = View::make('admin.products._price_item',[
            'priceModel' => $priceModel,
            'opened' => true
        ])->render();

        return $elem;
    }

    public function savePrice(Request $request)
    {
        $priceModel = ProductPrices::query()->where('id',$request->get('id'))->first();
        $priceModel->fill($request->except('_token'));
        $priceModel->save();

        $elem = View::make('admin.products._price_item',[
            'priceModel' => $priceModel,
            'opened' => true
        ])->render();

        return [
            'success' => true,
            'message' => 'Запись успешно сохранена!',
            'elem' => $elem
        ];
    }

    public function removePrice(Request $request)
    {
        $priceModel = ProductPrices::query()->where('id',$request->get('id'))->delete();

        return [
            'success' => true,
            'message' => 'Запись успешно удалено!'
        ];
    }

    public function generateRecommendationProducts()
    {
        $models = Products::query()->whereNull('recommendations')->get();

        foreach ($models as $model){
            $recommendationIds = [];

            if(isset($model->categories[0]->path)){
                $categories = $model->categories;

                foreach ($categories as $category){
                    if(count($recommendationIds) >= 4) break;
                    $products = $category->productsInSubCategories();

                    if($products){
                        foreach ($products as $product){
                            if(count($recommendationIds) >= 4) break;
                            if($product->id != $model->id && !in_array($product->id,$recommendationIds)){
                                $recommendationIds[] = $product->id;
                            }
                        }
                    }
                }
            }

            $model->recommendations = json_encode($recommendationIds);
            $model->save();
        }
    }
}
