<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\Products;
use App\Models\Services;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');

        $model = Services::query()
            ->orderBy('created_at', 'desc')
            ->where(function ($q) use ($status) {
                if ($status != '') {
                    $q->where('status', $status);
                }
            })
            ->paginate(20);

        return view('admin.services.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Services();

        $model->order = 0;

        $localizations = Langs::getLangsWithTitle();

        return view('admin.services.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Services();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Services::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.name'))));
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);
                $data['options'] = isset($data['options']) ? json_encode($data['options'],JSON_UNESCAPED_UNICODE) : json_encode([]);

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data,$constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('services.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('services.index')->with('success', 'Услуга успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Services $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Services $service)
    {
        $localizations = Langs::getLangsWithTitle();

        return view('admin.services.edit', [
            'model'         => $service,
            'data'          => $service->getTranslationsArray(),
            'localizations' => $localizations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* @var $model Services */
        $model = Services::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (! $model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);
                $data['options'] = isset($data['options']) ? json_encode($data['options'],JSON_UNESCAPED_UNICODE) : json_encode([]);

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data,$constructorData[$lang] ?? []));

                if (! $model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('services.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Услуга успешно обновлена!');
    }

    /**
     * @param Services $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Services $service)
    {
        $service->deleteTranslations();
        $service->delete();

        return redirect()->back()->with('success', 'Услугу успешно удалено!');
    }
}
