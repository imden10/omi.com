<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attributes;
use App\Models\Langs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');

        $model = Attributes::query()
            ->leftJoin('attribute_translations', 'attribute_translations.attributes_id','=','attributes.id')
            ->where('attribute_translations.lang',Langs::getDefaultLangCode())
            ->orderBy('attributes.order', 'asc')
            ->where(function ($q) use ($search) {
                if ($search != '') {
                    $q->where('attribute_translations.name','like', '%'.$search.'%');
                }
            })
            ->select('attributes.*')
            ->paginate(50);

        return view('admin.attributes.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Attributes();

        $model->order = 0;

        $localizations = Langs::getLangsWithTitle();

        return view('admin.attributes.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Attributes();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $model->translateOrNew($lang)->fill($request->input('page_data.' . $lang, []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('attributes.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('attributes.index')->with('success', 'Атрибут успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Attributes $attribute
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Attributes $attribute)
    {
        $localizations = Langs::getLangsWithTitle();

        return view('admin.attributes.edit', [
            'model'         => $attribute,
            'data'          => $attribute->getTranslationsArray(),
            'localizations' => $localizations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* @var $model Attributes */
        $model = Attributes::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            if (! $model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $model->translateOrNew($lang)->fill($request->input('page_data.' . $lang, []));

                if (! $model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Атрибут успешно обновлен!');
    }

    /**
     * @param Attributes $attribute
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Attributes $attribute)
    {
        $attribute->deleteTranslations();
        $attribute->delete();

        return redirect()->back()->with('success', 'Атрибут успешно удален!');
    }

    public function sort(Request $request)
    {
        $item = Attributes::query()->where('id',$request->get('id'))->first();
        $item->order = $request->get('order');
        $item->save();

        return redirect()->back()->with('success','Порядок сортировки обновлено!');
    }

    public function deleteSelected(Request $request)
    {
        $ids = json_decode($request->get('ids'),true);

        if(count($ids)){
            foreach ($ids as $id){
                $attribute = Attributes::query()->where('id',$id)->first();
                $attribute->deleteTranslations();
                $attribute->delete();
            }

            return redirect()->back()->with('success','Атрибуты успешно удалены !');
        }

        return redirect()->back()->with('error','Ошибка удаления!');
    }
}
