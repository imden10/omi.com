<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\MeterReading;
use App\Models\Receipts;
use App\Models\RequestDataRevision;
use App\Models\Requests;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * @param Request $r
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $r)
    {
        $user = User::query()->where('id', Auth::user()->id)->first();

        $request = Requests::query()
            ->where('user_id', $user->id)
            ->where('status', '<>', Requests::STATUS_SUCCESS)
            ->first();

        $revisions = false;

        if ($request) {
            $revisions = RequestDataRevision::query()
                ->where('request_id', $request->id)
                ->exists();
        } else {
            if(is_null($user->premises_owner)){
                return redirect()->route('profile.owner-check');
            }
        }

        if (($request && $request->status !== Requests::STATUS_ON_CHECK) || $revisions) {
            return redirect()->route('profile.request');
        }

        if ($r->has('mail-s')) {
            Session::flash('success', 'Ваша заявка прийнята. Дякуємо за довіру.');
        }

        return view('profile.home', [
            'request_status' => $request ? $request->status : null,
            'user'           => $user
        ]);
    }

    public function edit(Request $request)
    {
        if ($request->method() === 'GET') {
            $user = User::query()->where('id', Auth::id())->first();

            return view('profile.edit', [
                'user' => $user
            ]);
        } elseif ($request->method() === 'POST') {
            dd('post', $request->all());
        }
    }

    public function meterReadings(Request $request)
    {
        if ($request->method() === 'GET') {
            $user = User::query()->where('id', Auth::id())->first();

            return view('profile.meter_reading', [
                'user' => $user
            ]);
        } elseif ($request->method() === 'POST') {
            MeterReading::create([
                'user_id'    => $request->get('user_id'),
                'user_value' => $request->get('value')
            ]);

            return redirect()->route('profile')->with('success','Показники успішно відправлено');
        }
    }

    public function docs(Request $request)
    {
        if ($request->method() === 'GET') {
            $user = User::query()->where('id', Auth::id())->first();

            return view('profile.docs', [
                'user' => $user
            ]);
        } elseif ($request->method() === 'POST') {
            dd('post', $request->all());
        }
    }

    public function receipts(Request $request)
    {
        if ($request->method() === 'GET') {
            $user = User::query()->where('id', Auth::id())->first();

            return view('profile.receipts', [
                'user' => $user
            ]);
        } elseif ($request->method() === 'POST') {
            /* @var $receipt Receipts */
            $receipt = Receipts::query()->where('id', $request->get('id'))->first();

            if (!$receipt)
                return redirect()->back()->with('error', 'Виникла помилка!');

            $receipt->status = Receipts::STATUS_PAID;
            $receipt->save();

            return redirect()->back()->with('success', 'Ви підтвердили оплату успішно');
        }
    }

}
