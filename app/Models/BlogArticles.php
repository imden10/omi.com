<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BlogArticles
 * @package App\Models
 *
 * @property integer $id
 * @property string $slug
 * @property integer $status
 * @property integer $views
 * @property integer $user_id
 * @property Carbon $public_date
 * @property string $image
 * @property string $image_thumb
 * @property integer $product_category_id
 *
 * @property BlogTags[] $tags
 * @property Category $productCategory
 */
class BlogArticles extends Model
{
    use HasFactory;
    use Sluggable;
    use Translatable;

    protected $table = 'blog_articles';

    public $translationModel = 'App\Models\Translations\BlogArticleTranslation';

    public $translatedAttributes = [
        'name',
        'excerpt',
        'text',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    protected $fillable = [
        'slug',
        'views',
        'user_id',
        'image',
        'image_thumb',
        'public_date',
        'product_category_id'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активна',
            self::STATUS_ACTIVE     => 'Активна'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(BlogTags::class, 'blog_article_tag', 'blog_article_id', 'blog_tag_id');
    }

    public function productCategory()
    {
        return $this->hasOne(Category::class,'id','product_category_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE)
            ->where('public_date', '<=', Carbon::now());
    }
}
