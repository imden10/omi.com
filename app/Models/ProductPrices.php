<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductPrices
 * @package App\Models
 *
 * @property integer $product_id
 * @property integer $count_position
 * @property integer $count_position_unit_id
 * @property integer $retail_count
 * @property integer $retail_count_unit_id
 * @property integer $retail_price
 * @property integer $wholesale_count
 * @property integer $wholesale_count_unit_id
 * @property integer $wholesale_price
 * @property integer $order
 * @property boolean $status
 * @property boolean $is_main
 */
class ProductPrices extends Model
{
    use HasFactory;

    protected $table = 'product_prices';

    public $timestamps = false;

    protected $guarded = [];

    protected $casts = [
        'status' => 'boolean',
        'is_main' => 'boolean'
    ];

    const STATUS_NOT_ACTIVE = false;
    const STATUS_ACTIVE     = true;

    const UNITS = [
        'ru' => [
            '1' => 'л',
            '2' => 'шт',
            '3' => 'г',
        ],
        'uk' => [
            '1' => 'л',
            '2' => 'шт',
            '3' => 'г',
        ],
        'en' => [
            '1' => 'L',
            '2' => 'PC',
            '3' => 'gr',
        ],
    ];

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активно',
            self::STATUS_ACTIVE     => 'Активно'
        ];
    }

    /**
     * @param null $lang
     * @return array
     */
    public static function getUnits($lang = null): array {
        if(! $lang)
            $lang = Langs::getDefaultLangCode();

        return self::UNITS[$lang];
    }
}
