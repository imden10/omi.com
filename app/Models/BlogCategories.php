<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * Class BlogCategories
 * @package App\Models
 *
 * @property integer $id
 * @property string $slug
 * @property integer $status
 * @property string $name
 * @property string $excerpt
 * @property string $text
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property BlogArticles[] $articles
 */
class BlogCategories extends Model
{
    use NodeTrait, Sluggable {
        Sluggable::replicate insteadof NodeTrait;
        NodeTrait::replicate insteadof Sluggable;
    }

    protected $table = 'blog_categories';

    protected $fillable = [
        'name',
        'slug',
        'status',
        'text',
        'excerpt',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активна',
            self::STATUS_ACTIVE     => 'Активна'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles()
    {
        return $this->belongsToMany(BlogArticles::class, 'blog_article_tag', 'blog_article_id', 'blog_tag_id');
    }
}
