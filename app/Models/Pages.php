<?php

namespace App\Models;

use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Traits\Constructorable;
use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Modules\Constructor\Contracts\HasConstructor;

/**
 * Class Pages
 * @package App
 *
 * @property string $slug
 * @property integer $status
 * @property integer $template
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property array $getTypes
 * @property array $getStatuses
 */
class Pages extends Model
{
    use Sluggable;
    use Translatable;

    protected $table = 'pages';

    public $translationModel = 'App\Models\Translations\PagesTranslation';

    public $translatedAttributes = [
        'title',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    protected $fillable = [
        'slug',
        'status',
        'template'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активная',
            self::STATUS_ACTIVE     => 'Активная'
        ];
    }
}
