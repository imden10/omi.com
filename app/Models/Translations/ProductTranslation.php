<?php

namespace App\Models\Translations;

use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Contracts\HasConstructor;
use App\Modules\Constructor\Traits\Constructorable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductTranslation
 * @package App\Models\Translations
 *
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 */
class ProductTranslation extends Model implements HasConstructor
{
    use HasFactory;
    use Constructorable;


    protected $table = 'product_translations';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    public function fill(array $attributes = [])
    {
        $this->fillConstructorable($attributes);

        return parent::fill($attributes);
    }

    /**
     * @inheritDoc
     */
    public function constructorComponents(): array
    {
        return [
//            'products' => ComponentCollections::products(),
            'widget'   => ComponentCollections::widget(),
        ];
    }
}
