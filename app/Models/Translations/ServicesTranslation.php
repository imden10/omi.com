<?php

namespace App\Models\Translations;

use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Contracts\HasConstructor;
use App\Modules\Constructor\Traits\Constructorable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicesTranslation extends Model implements HasConstructor
{
    use HasFactory;

    use Constructorable;

    protected $table = 'services_translations';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'excerpt',
        'options',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    private string $entityAttribute = 'services_id';

    /**
     * @param array $attributes
     * @return Model
     */
    public function fill(array $attributes = [])
    {
        $this->fillConstructorable($attributes);

        return parent::fill($attributes);
    }

    /**
     * @inheritDoc
     */
    public function constructorComponents(): array
    {
        return [
            'simple-text'          => ComponentCollections::simpleText(),
            'text-2-columns'       => ComponentCollections::text2Columns(),
            'quotes'               => ComponentCollections::quotes(),
            'image-and-text'       => ComponentCollections::imageAndText(),
            'full-image-and-text'  => ComponentCollections::fullImageAndText(),
            'list'                 => ComponentCollections::list(),
            'statistics'           => ComponentCollections::statistics(),
            'advantages'           => ComponentCollections::advantages(),
            'button'               => ComponentCollections::button(),
            'button-order-service' => ComponentCollections::buttonOrderService(),
            'gallery'              => ComponentCollections::gallery(),
            'widget'               => ComponentCollections::widget(),
        ];
    }
}
