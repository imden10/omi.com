<?php

namespace App\Models\Translations;

use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Contracts\HasConstructor;
use App\Modules\Constructor\Traits\Constructorable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogArticleTranslation extends Model implements HasConstructor
{
    use HasFactory;
    use Constructorable;

    protected $table = 'blog_article_translations';

    private string $entityAttribute = 'blog_articles_id';


    protected $fillable = [
        'name',
        'excerpt',
        'text',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    /**
     * @param array $attributes
     * @return Model
     */
    public function fill(array $attributes = [])
    {
        $this->fillConstructorable($attributes);

        return parent::fill($attributes);
    }

    public $timestamps = false;

    /**
     * @inheritDoc
     */
    public function constructorComponents(): array
    {
        return [
            'simple-text'          => ComponentCollections::simpleText(),
            'text-2-columns'       => ComponentCollections::text2Columns(),
            'quotes'               => ComponentCollections::quotes(),
            'image-and-text'       => ComponentCollections::imageAndText(),
            'full-image-and-text'  => ComponentCollections::fullImageAndText(),
            'list'                 => ComponentCollections::list(),
            'statistics'           => ComponentCollections::statistics(),
            'advantages'           => ComponentCollections::advantages(),
            'button'               => ComponentCollections::button(),
            'gallery'              => ComponentCollections::gallery(),
            'widget'               => ComponentCollections::widget(),
        ];
    }
}
