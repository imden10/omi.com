<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AttributesCategory extends Pivot
{
    use HasFactory;

    protected $table = 'attributes_category';

    public $timestamps = false;

    protected $guarded = [];
}
