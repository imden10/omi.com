<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CategoryProducts extends Pivot
{
    use HasFactory;

    protected $table = 'category_products';

    public $timestamps = false;

    protected $guarded = [];
}
