<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Attributes
 * @package App\Models
 *
 * @property integer $order
 */
class Attributes extends Model
{
    use HasFactory;

    use Translatable;

    protected $table = 'attributes';

    public $translationModel = 'App\Models\Translations\AttributeTranslation';

    public $translatedAttributes = [
        'name'
    ];

    protected $fillable = [
        'order',
    ];

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }
}
