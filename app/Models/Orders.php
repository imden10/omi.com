<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Orders
 * @package App\Models
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $product_id
 * @property integer $status
 *
 * @property Products $product
 */
class Orders extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $guarded = [];

    const STATUS_NEW       = 0;
    const STATUS_VIEW      = 1;
    const STATUS_PROCESSED = 2;

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NEW       => 'Новый',
            self::STATUS_VIEW      => 'Просмотрен',
            self::STATUS_PROCESSED => 'Обработан'
        ];
    }

    public function product()
    {
        return $this->hasOne(Products::class,'id','product_id');
    }
}
