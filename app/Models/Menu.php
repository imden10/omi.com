<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Menu extends Model
{
    use NodeTrait;
    use Translatable;

    protected $table = "menu";

    public $translationModel = 'App\Models\Translations\MenuTranslation';

    public $translatedAttributes = [
        'name',
        'url'
    ];

    protected $fillable = [
        'visibility',
        'parent_id ',
        'tag',
        'const',
        'type',
        'model_id'
    ];

    public $timestamps = false;

    const VISIBILITY_OFF = 0;
    const VISIBILITY_ON  = 1;

    const TYPE_ARBITRARY        = 0;
    const TYPE_PAGE             = 1;
    const TYPE_BLOG             = 2;
    const TYPE_PRODUCT_CATEGORY = 3;
    const TYPE_PRODUCT          = 4;
    const TYPE_SERVICE          = 5;

    public static function getVisibility(): array
    {
        return [
            self::VISIBILITY_OFF => 'Не показувати',
            self::VISIBILITY_ON  => 'Показувати'
        ];
    }

    /**
     * @return array
     */
    public static function getTags(): array
    {
        return self::query()
                ->where('const', 1)
                ->pluck('tag', 'tag')
                ->toArray() ?? [];
    }

    public static function getTypes(): array
    {
        return [
            self::TYPE_PAGE             => 'Страницы',
            self::TYPE_BLOG             => 'Блог',
            self::TYPE_PRODUCT_CATEGORY => 'Категории продуктов',
            self::TYPE_PRODUCT          => 'Продукты',
            self::TYPE_SERVICE          => 'Услуги',
            self::TYPE_ARBITRARY        => 'Произвольные ссылки'
        ];
    }

    public static function getTypesModel(): array
    {
        return [
            self::TYPE_PAGE             => [
                'rel'        => 'page',
                'name'       => 'title',
                'url_prefix' => ''
            ],
            self::TYPE_BLOG             => [
                'rel'        => 'blog',
                'name'       => 'title',
                'url_prefix' => '/blog/'
            ],
            self::TYPE_PRODUCT_CATEGORY => [
                'rel'        => 'product_category',
                'name'       => 'title',
                'url_prefix' => '/catalog/'
            ],
            self::TYPE_PRODUCT          => [
                'rel'        => 'product',
                'name'       => 'name',
                'url_prefix' => ''
            ],
            self::TYPE_SERVICE          => [
                'rel'        => 'service',
                'name'       => 'name',
                'url_prefix' => '/service/'
            ]
        ];
    }

    public function product()
    {
        return $this->hasOne(Products::class, 'id', 'model_id');
    }

    public function page()
    {
        return $this->hasOne(Pages::class, 'id', 'model_id');
    }

    public function service()
    {
        return $this->hasOne(Services::class, 'id', 'model_id');
    }

    public function blog()
    {
        return $this->hasOne(BlogArticles::class, 'id', 'model_id');
    }

    public function productCategory()
    {
        return $this->hasOne(Category::class, 'id', 'model_id');
    }
}
