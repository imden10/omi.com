<?php

namespace App\Models;

use App\Models\Translations\AttributeTranslation;
use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Products
 * @package App\Models
 *
 * @property integer $id
 * @property integer $is_stock_widget
 * @property string $slug
 * @property string $image
 * @property string $order
 * @property string $status
 * @property string $recommendations
 *
 * @property Category[] $categories
 * @property ProductPrices[] $prices
 */
class Products extends Model
{
    use HasFactory;

    use Sluggable;
    use Translatable;
    use SluggableScopeHelpers;

    protected $table = 'products';

    public $translationModel = 'App\Models\Translations\ProductTranslation';

    public $translatedAttributes = [
        'name',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    protected $fillable = [
        'slug',
        'image',
        'order',
        'status',
        'is_stock_widget',
        'recommendations'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    const STATUS_NOT_ACTIVE = false;
    const STATUS_ACTIVE     = true;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активный',
            self::STATUS_ACTIVE     => 'Активный'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class)->using(CategoryProducts::class);
    }

    /**
     * @return string
     */
    public function categoriesToString()
    {
        return implode(', ', $this->categories->pluck('title')->toArray());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(ProductPrices::class,'product_id','id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @return BelongsToMany
     */
    public function attributes()
    {
        return $this->hasMany(ProductAttributes::class,'product_id','id')
            ->where('lang',app()->getLocale());
    }
}
