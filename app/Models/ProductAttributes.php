<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductAttributes
 * @package App\Models
 *
 * @property integer $product_id
 * @property integer $attribute_id
 * @property string $value
 * @property string $slug
 * @property string $lang
 */
class ProductAttributes extends Model
{
    use HasFactory;

    protected $table = 'product_attributes';

    public $timestamps = false;

    protected $guarded = [];
}
