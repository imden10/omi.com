<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Services
 * @package App\Models
 *
 * @property string $image
 * @property string $slug
 * @property integer $status
 * @property integer $order
 */

class Services extends Model
{
    use HasFactory;

    use Sluggable;
    use Translatable;

    protected $table = 'services';

    public $translationModel = 'App\Models\Translations\ServicesTranslation';

    public $translatedAttributes = [
        'name',
        'description',
        'excerpt',
        'options',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    protected $fillable = [
        'image',
        'slug',
        'status',
        'order'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активная',
            self::STATUS_ACTIVE     => 'Активная'
        ];
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }
}
