<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * Class Category
 * @package App\Models
 *
 * @property string $slug
 * @property string $image
 * @property integer $parent_id
 * @property integer $order
 * @property boolean $status
 *
 * @property Attributes[] $attributes
 * @property Products[] $products
 * @property Products[] $productsInSubCategories
 */
class Category extends Model
{
    use HasFactory;

    use Sluggable;
    use Translatable;
    use SluggableScopeHelpers;

    protected $table = 'category';

    public $translationModel = 'App\Models\Translations\CategoryTranslation';

    public $translatedAttributes = [
        'title',
        'excerpt',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'options'
    ];

    protected $fillable = [
        'slug',
        'image',
        'order',
        'status',
        'parent_id'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    const STATUS_NOT_ACTIVE = false;
    const STATUS_ACTIVE     = true;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активная',
            self::STATUS_ACTIVE     => 'Активная'
        ];
    }


    public function attributes()
    {
        return $this->belongsToMany(Attributes::class)->using(AttributesCategory::class);
    }

    /* Generate tree structure data */
    public function treeStructure()
    {
        $collection = $this->query()
            ->get()
            ->toArray();

        $normalize = [];

        foreach ($collection as $item) {
            $normalize[$item['parent_id']][$item['id']] = $item;
        }
        if(!empty($normalize)) {
            $node = $normalize[0];

            $this->treeNode($node, $normalize);
        } else {
            $node = $normalize;
        }

        return collect($node);
    }

    public function treeNode(&$node, $normalize)
    {
        foreach($node as $key => $item) {
            if (!isset($item['children'])) {
                $node[$key]['children'] = [];
            }

            if (array_key_exists($key, $normalize)) {
                $node[$key]['children'] = $normalize[$key];
                $this->treeNode($node[$key]['children'], $normalize);
            }
        }
    }

    public function parent() {
        return $this->belongsTo(self::class, 'parent_id');
    }

    private function getCategories($cat, $res = [])
    {
        if(isset($cat->parent->title)){
            $res[] = $cat->parent->title;
            return $this->getCategories($cat->parent,$res);
        }

        return $res;
    }

    public function getNameWithPath()
    {
        $arr = $this->getCategories($this);

        $arr = array_reverse($arr);

        $res = '';

        foreach ($arr as $item){
            $res .= $item . ' > ';
        }

        $res .= $this->title;

        return $res;
    }

    /**
     * Продукты в текущей категории, без учета вложинных подкатегория
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Products::class);
    }

    /**
     * Подсчет количества продуктов в категории
     * @return int
     */
    public function countProducts(): int
    {
        return $this->products()->active()->count();
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    /**
     * Подсчет количества продуктов в категории с учетом всех подкатегорий
     * @return int
     */
    public function countProductsInSubCategories(): int
    {
        $self = $this;
        $count = 0;
        $count += $this->countProducts() +
                $self->childs()->get()
                    ->sum(function (Category $category) {
                        return $category->countProductsInSubCategories();
                    });
        return $count;
    }

    public function getAllChildCategoryIds($children = null, $ids = [])
    {
        if($children){
            foreach ($children->get() as $child){
                $ids[] = $child->id;
                $ids = $this->getAllChildCategoryIds( $child->childs(), $ids);
            }

        }

        return $ids;
    }

    /**
     * продукты в категории с учетом всех подкатегорий
     */
    public function productsInSubCategories()
    {
        $ids = [];
        $ids[] = $this->id;

        $ids = array_merge($ids,$this->getAllChildCategoryIds($this->childs()));

        $productIds = CategoryProducts::query()->whereIn('category_id',$ids)->pluck('products_id')->toArray();

        return Products::query()
            ->leftJoin('product_translations','product_translations.products_id','=','products.id')
            ->where('product_translations.lang',Langs::getDefaultLangCode())
            ->where('products.status', Products::STATUS_ACTIVE)
            ->whereIn('products.id',$productIds)
            ->select([
                'products.*',
                'product_translations.name'
            ])
            ->get();
    }

    /**
     * Подсчет количества категорий с учетом всех подкатегорий
     * @return int
     */
    public function countSubCategories(): int
    {
        $self = $this;
        $count = 0;
        $count += $this->childs()->count() +
            $self->childs()->get()
                ->sum(function (Category $category) {
                    return $category->countSubCategories();
                });
        return $count;
    }
}
