<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('person_name', 255)->nullable()->comment('Ім’я');
            $table->string('person_lastname', 255)->nullable()->comment('Прізвище');
            $table->string('person_middlename', 255)->nullable()->comment('По-батькові');
            $table->string('person_inn', 10)->nullable()->comment('ІПН');
            $table->tinyInteger('object_type')->nullable()->comment('Вид об’єкта');
            $table->tinyInteger('consumption_type')->nullable()->comment('Тип споживання');
            $table->tinyInteger('operator_grm')->nullable()->comment('Найменування оператора ГРМ');
            $table->string('code_eic', 16)->nullable()->comment('ЕІС-код');
            $table->tinyInteger('presence_meter')->nullable()->comment('Наявність лічильника');
            $table->string('region', 255)->nullable()->comment('Область');
            $table->string('city', 255)->nullable()->comment('Населений пункт');
            $table->string('district', 255)->nullable()->comment('Район');
            $table->string('street', 255)->nullable()->comment('Вулиця');
            $table->string('house', 255)->nullable()->comment('Будинок');
            $table->string('zip_code', 10)->nullable()->comment('Індекс');
            $table->tinyInteger('tariff')->nullable()->comment('Тариф');
            $table->tinyInteger('subsidy')->nullable()->comment('Наявність пільг/субсидій');
            $table->string('promo_code', 255)->nullable()->comment('Промокод');
            $table->integer('number_residents')->default(0)->comment('Кількість проживаючих');
            $table->string('request_number')->nullable()->unique()->comment('Номер заявки');
            $table->tinyInteger('status')->default(1)->comment('Статус');
            $table->dateTime('change_status_at')->nullable()->comment('Дата зміни статуса');
            $table->bigInteger('change_status_user_id')->nullable()->comment('ID користувача, який змінив статус');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
